// https://umijs.org/config/
import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';
const { REACT_APP_ENV } = process.env;
export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  layout: {
    name: 'Ant Design Pro',
    locale: true,
  },
  locale: {
    // default zh-CN
    default: 'zh-CN',
    antd: true,
    // default true, when it is true, will use `navigator.language` overwrite default
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: '/user',
      layout: false,
      routes: [
        {
          name: 'login',
          path: '/user/login',
          component: './user/login',
        },
      ],
    },
    {
      path: '/welcome',
      component: './Welcome',
    },
    {
      path: '/sys',
      name: '系统管理',
      access: 'canAdmin',
      icon: 'setting',
      routes: [
        {
          path: '/sys/user',
          name: '用户管理',
          access: 'canAdmin',
          icon: 'UserOutlined',
          component: './system/user',
        },
        {
          path: '/sys/role',
          name: '角色管理',
          access: 'canAdmin',
          icon: 'TeamOutlined',
          component: './system/role',
        },
        {
          path: '/sys/menu',
          name: '菜单管理',
          access: 'canAdmin',
          icon: 'UnorderedListOutlined',
          component: './system/menu',
        },
        {
          path: '/sys/dict',
          name: '字典管理',
          access: 'canAdmin',
          icon: 'ReconciliationOutlined',
          component: './system/dict',
        },
        {
          path: '/sys/dict/detail/:parentId/:parentName',
          icon: 'ReconciliationOutlined',
          component: './system/dict/components/details',
        },
        {
          component: './404',
        },
      ],
    },
    {
      path: '/tool',
      name: '系统工具',
      access: 'canAdmin',
      routes: [
        {
          path: '/tool/code/generator',
          name: '代码生成',
          access: 'canAdmin',
          component: './tool/creator',
        },
        {
          path: '/tool/cron/config',
          name: '定时任务',
          access: 'canAdmin',
          component: './tool/cron',
        },
        {
          path: '/tool/upload',
          name: '文件上传',
          access: 'canAdmin',
          component: './tool/upload',
        },
        {
          path: '/tool/swagger/api',
          name: '接口文档',
          access: 'canAdmin',
          component: './tool/swagger',
        },
        {
          component: './404',
        },
      ],
    },
    {
      path: '/',
      redirect: '/welcome',
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
});
