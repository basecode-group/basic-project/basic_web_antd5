import { request } from 'umi';

// 查询当前登录用户
export async function query(): Promise<API.ResultPoJo<System.SysUser>> {
  // @ts-ignore
  return request('/api/sys/user/get', {
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询当前登录用户
export async function queryCurrent() {
  const sysUser = await query();
  if (sysUser.code === '000') {
    const user = sysUser.result;
    return {
      // avatar: 'http://47.56.198.158/static/5.e147bdeb.png',
      avatar: user?.photo,
      access: 'admin',
      name: user.name,
      userid: user.id,
    };
  }
  return {};
}

export async function queryNotices(): Promise<any> {
  return request<{ data: API.NoticeIconData[] }>('/api/notices');
}

// 获取用户列表
export async function getUserList(params: any): Promise<API.ResultPoJo<any>> {
  const data = await request('/api/sys/user', {
    params: {
      ...params,
      pageNum: params?.current,
      pageSize: params?.pageSize,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  const page = data.result;
  return {
    data: page?.records,
    current: page?.current,
    total: page?.total,
    success: data.code === '000',
  };
}

// 保存用户
export async function saveUser(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/sys/user', {
    method: 'post',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除用户
export async function userDelete(id: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/sys/user/${id}`, {
    method: 'delete',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 重置密码
export async function resetPwd(id: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/sys/user/reset/pwd/${id}`, {
    method: 'put',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 根据用户Id 获取用户
export async function getUserId(id: string): Promise<API.ResultPoJo<System.SysUser>> {
  // @ts-ignore
  return request(`/api/sys/user/${id}`, {
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 用户更新
export async function userUpdate(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/sys/user/${params.id}`, {
    method: 'put',
    data: params,
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 更新密码
export async function updatePwd(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/sys/user/update/pwd', {
    method: 'put',
    data: params,
    requestType: 'form',
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
