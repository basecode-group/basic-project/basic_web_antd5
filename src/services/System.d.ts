declare namespace System {
  // 参数
  export interface SystemParams {
    pageSize?: number;
    currentPage?: number;
    filter?: { [key: string]: any[] };
    sorter?: { [key: string]: any };
  }

  // 系统用户
  export interface SysUser {
    id: string;
    loginName: string;
    name: string;
    email: string;
    mobile: string;
    userType: string;
    photo: string;
    tails: {
      access_token: string;
      userTypeLabel: string;
      roleIds: string[];
    };
  }

  // 字典对象
  interface SysDict {
    id: string;
    parentId: string;
    type: string;
    label: string;
    value: string;
    sort: number;
    description: string;
    createDate: string;
  }

  // 排序对象
  interface SysSort {
    maxSort: number;
  }

  // 菜单对象
  interface SysMenu {
    id: string;
    parentId: string;
    name: string;
    sort: number;
    href: string;
    target: string;
    icon: string;
    isShow: string;
    permission: string;
    children: SysMenu[];
  }

  // 系统角色
  interface SysRole {
    id: string;
    name: string;
    enname: string;
    roleType: string;
    useable: string;
    tails: {
      roleTypeLabel: string;
    };
  }

  // 角色授权
  interface UserAuth4Role {
    checked: string[];
    userList: {
      key: string;
      name: string;
      loginName: string;
    }[];
  }
}
