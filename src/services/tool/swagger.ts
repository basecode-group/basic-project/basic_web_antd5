import { request } from 'umi';

// 获取 swagger 地址
export function getSwaggerUrl(): Promise<API.ResultPoJo<any[]>> {
  // @ts-ignore
  return request('/api/swagger/resources/get', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询jobs
export async function getSwaggerEvnList(params?: any) {
  const data = await request<API.ResultPoJo<any>>(
    '/api/sys/dict/get/children/list/api_url_config',
    {
      params: {
        ...params,
        pageNum: params?.current,
        pageSize: params?.pageSize,
      },
      // token
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    },
  );
  return {
    data: data.result,
    success: data.code === '000',
  };
}

// 获取swagger环境，最大排序
export function getSwaggerEvnMaxSort(): Promise<API.ResultPoJo<System.SysSort>> {
  // @ts-ignore
  return request('/api/sys/dict/max/sort', {
    params: { type: 'api_url_config' },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 获取swagger 环境配置父类
export function getSwaggerEvnParent(): Promise<API.ResultPoJo<System.SysDict>> {
  // @ts-ignore
  return request('/api/sys/dict/get/parent/api_url_config', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
