import { request } from 'umi';

// 查询列表
export async function getCronList(params?: any) {
  const data = await request<API.ResultPoJo<API.Page<System.SysDict>>>('/api/cron', {
    params: {
      ...params,
      pageNum: params?.current,
      pageSize: params?.pageSize,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  const page = data.result;
  return {
    data: page?.records,
    current: page?.current,
    total: page?.total,
    success: data.code === '000',
  };
}

// 获取定时任务数据
export function getCronSelectData(): Promise<API.ResultPoJo<API.DictMap>> {
  // @ts-ignore
  return request('/api/cron/get/system/cron/data', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 获取下期定时任务
export function getCronNextDate(cron: string): Promise<API.ResultPoJo<string[]>> {
  // @ts-ignore
  return request(`/api/quartz/next/cron/date/`, {
    params: {
      cron,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 保存
export function saveCron(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/cron', {
    method: 'post',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除
export function deleteCron(ids: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/cron/${ids}`, {
    method: 'delete',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询定时任务
export function getCronById(id: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/cron/${id}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 更新
export function updateCron(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/cron/${params.id}`, {
    method: 'put',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

//  quartz API

// 创建定时任务
export function jobCreate(jobName: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/quartz/create/schedule/${jobName}`, {
    method: 'post',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 暂停定时任务
export function jobPause(jobName: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/quartz/pause/schedule/${jobName}`, {
    method: 'post',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 恢复定时任务
export function jobResume(jobName: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/quartz/resume/schedule/${jobName}`, {
    method: 'post',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 执行定时任务
export function jobRun(jobName: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/quartz/runOnce/schedule/${jobName}`, {
    method: 'post',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除定时任务
export function jobDelete(jobName: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/quartz/delete/schedule/${jobName}`, {
    method: 'delete',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 更新定时任务
export function jobUpdate(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/quartz/update/schedule/${params.jobName}`, {
    method: 'put',
    data: params,
    requestType: 'form',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询jobs
export async function getSysJobs(params?: any) {
  const data = await request<API.ResultPoJo<any>>('/api/quartz/get/jobs', {
    params: {
      ...params,
      pageNum: params?.current,
      pageSize: params?.pageSize,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  return {
    data: data.result,
    success: data.code === '000',
  };
}

// 查询jobs
export async function getMonitorJobs(params?: any) {
  const data = await request<API.ResultPoJo<any>>('/api/quartz/get/jobs/monitor', {
    params: {
      ...params,
      pageNum: params?.current,
      pageSize: params?.pageSize,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  return {
    data: data.result,
    success: data.code === '000',
  };
}
