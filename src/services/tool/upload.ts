import { request } from 'umi';

/**
 * 更新文件上传配置
 * @param params
 */
export function uploadConfigUpdate(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/file/upload/config/update', {
    method: 'put',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 获取文件上传路径
export function getUploadPath(path: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/file/upload/path/tree', {
    params: { path },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 初始化路径
export function getUploadPathInit(path: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/file/upload/path/tree/init', {
    params: { path },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询代码生成列表
export async function getUploadFileList(params?: any) {
  const data = await request<API.ResultPoJo<API.Page<System.SysDict>>>(
    '/api/file/upload/file/list',
    {
      params: {
        ...params,
        pageNum: params?.current,
        pageSize: params?.pageSize,
      },
      // token
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    },
  );
  const page = data.result;
  return {
    data: page?.records,
    current: page?.current,
    total: page?.total,
    success: data.code === '000',
  };
}

// 删除文件
export function delFile(path: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/file/upload/delete', {
    method: 'delete',
    params: { path },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
