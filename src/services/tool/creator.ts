import { request } from 'umi';

// 查询代码生成列表
export async function getCreatorList(params?: any) {
  const data = await request<API.ResultPoJo<API.Page<System.SysDict>>>('/api/api/creator', {
    params: {
      ...params,
      pageNum: params?.current,
      pageSize: params?.pageSize,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  const page = data.result;
  return {
    data: page?.records,
    current: page?.current,
    total: page?.total,
    success: data.code === '000',
  };
}

// 查询类型
export function getCreatorFileTypeData(): Promise<API.ResultPoJo<API.DictMap>> {
  // @ts-ignore
  return request<API.ResultPoJo<API.DictMap>>('/api/api/creator/file/type/data', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询数据库表
export function getTableList(): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/api/datasource/table/get`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 保存
export function saveCreator(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/api/creator', {
    method: 'post',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 更新
export function updateCreator(params: any): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/api/creator/${params.id}`, {
    method: 'put',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除
export function deleteCreator(ids: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/api/creator/${ids}`, {
    method: 'delete',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询
export function getCreatorById(id: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/api/creator/${id}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 代码生成
export function generatorCode(ids: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/api/creator/generator/${ids}`, {
    method: 'post',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
