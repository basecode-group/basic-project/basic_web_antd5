declare namespace Tool {
  // 代码生成对象
  export interface SysCreator {
    id: string;
    name: string;
    tableName: string;
    author: string;
    outPutDir: string;
    packageDir: string;
    createDate: string;
  }

  // 定时任务对象
  export interface SysCron {
    id: string;
    name: string;
    cron: string;
    status: '0' | '1';
    mark: string;
    remarks: string;
    tails: {
      cronStatus?: API.DictMap;
      currentCron?: string;
      oldCron?: string;
    };
  }
}
