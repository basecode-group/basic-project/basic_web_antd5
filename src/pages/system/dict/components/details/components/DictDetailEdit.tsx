import React, { useState, useEffect } from 'react';
import { dictEdit, getDictById } from '@/services/system/dict';
import { Button, Modal, Spin, Form, Input, InputNumber, message } from 'antd';
import { EditOutlined } from '@ant-design/icons';

// 定义传参
interface DictDetailEditParams {
  refresh: () => void;
  dictId: string;
  parentName: string;
}

/**
 * 编辑字典
 *
 * @author zhangby
 * @date 22/9/20 4:16 pm
 */
const DictDetailEdit: React.FC<DictDetailEditParams> = (props) => {
  const { refresh, dictId, parentName, children } = props;
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const [form] = Form.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    try {
      setLoading(true);
      const dict = await getDictById(dictId);
      form.setFieldsValue(dict.result);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = async () => {
    form.validateFields().then(async (values) => {
      try {
        setLoading(true);
        // 新建字典
        const data = await dictEdit({ id: dictId, ...values });
        if (data.code === '000') {
          message.success('编辑字典成功');
          setLoading(false);
          setVisible(false);
          refresh();
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <span style={{ marginLeft: 10, cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary" shape="circle">
            <EditOutlined />
          </Button>
        )}
      </span>
      <Modal
        title="编辑字典"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={600}
      >
        <Spin spinning={loading}>
          <Form {...layout} form={form} initialValues={{}}>
            <Form.Item label="父类名称">
              <Input disabled value={parentName} />
            </Form.Item>
            <Form.Item
              name="label"
              label="字典名称"
              rules={[{ required: true, message: '字典名称不能为空' }]}
            >
              <Input placeholder="字典名称" />
            </Form.Item>
            <Form.Item
              name="value"
              label="字典类型"
              rules={[{ required: true, message: '字典类型不能为空' }]}
            >
              <Input placeholder="字典类型" />
            </Form.Item>
            <Form.Item name="sort" label="排序" initialValue={10}>
              <InputNumber min={0} step={1} precision={0} />
            </Form.Item>
            <Form.Item name="description" label="描述">
              <Input.TextArea placeholder="字典描述" autoSize={{ minRows: 3, maxRows: 5 }} />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};

// Form 布局
const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};
export default DictDetailEdit;
