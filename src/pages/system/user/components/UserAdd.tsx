import React, { useState, useEffect } from 'react';
import { Button, Drawer, Spin, Form, Input, Select, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import UserPhoto from '@/pages/common/userPhoto/UserPhoto';
import { getDictList4Type } from '@/services/system/dict';
import { getRoleSelectData } from '@/services/system/role';
import { saveUser } from '@/services/system/user';

const { Option } = Select;

// 定义传参
interface UserAddParams {
  refresh: () => void;
}

/**
 * 用户新增
 *
 * @author zhangby
 * @date 24/9/20 6:24 pm
 */
const UserAdd: React.FC<UserAddParams> = (props) => {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [userTypeList, setUserTypeList] = useState<API.DictMap[]>();
  const [roleSelectData, setRoleSelectData] = useState<API.DictMap[]>();

  const [form] = Form.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    // 查询数据
    try {
      setLoading(true);
      // eslint-disable-next-line no-shadow
      const userTypeList = await getDictList4Type('user_type');
      if (userTypeList.code === '000') {
        setUserTypeList(userTypeList.result);
      }
      // 查询用户角色
      const roleSelectDataDB = await getRoleSelectData();
      if (roleSelectDataDB.code === '000') {
        setRoleSelectData(roleSelectDataDB.result);
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    form.validateFields().then(async (values) => {
      try {
        setLoading(true);
        const params = { ...values, roleId: values.roleIds.join(',') };
        const data = await saveUser(params);
        if (data.code === '000') {
          message.success('新建用户成功');
          setVisible(false);
          setLoading(false);
          props.refresh();
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  // @ts-ignore
  return (
    <>
      <Button type="primary" onClick={onPreInit}>
        <PlusOutlined />
        新建用户
      </Button>
      {/* 弹出窗口 */}
      <Drawer
        title="新建用户"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          {/* form 表单 */}
          <Form {...layout} form={form}>
            <Form.Item name="photo" label="" style={{ marginLeft: 280 }}>
              <UserPhoto isEdit size={60} />
            </Form.Item>
            <Form.Item
              name="name"
              label="用户名"
              rules={[{ required: true, message: '用户名不能为空' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="loginName"
              label="登录名"
              rules={[{ required: true, message: '登录名不能为空' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="mobile" label="联系方式">
              <Input />
            </Form.Item>
            <Form.Item name="email" label="电子邮箱">
              <Input />
            </Form.Item>
            <Form.Item name="userType" label="用户类型">
              <Select placeholder="选择用户类型">
                {userTypeList?.map((item, key) => (
                  <Option value={item.value} key={key}>
                    {item.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="roleIds" label="所属角色">
              <Select placeholder="选择所属角色" mode="multiple">
                {roleSelectData?.map((item, key) => (
                  <Option value={item.value} key={key}>
                    {item.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="password"
              label="密码"
              rules={[
                {
                  required: true,
                  message: '请输入密码',
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="confirm"
              label="确认密码"
              dependencies={['password']}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: '请输入确认密码',
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    // eslint-disable-next-line prefer-promise-reject-errors
                    return Promise.reject('密码与确认密码不正确');
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
          </Form>
        </Spin>
      </Drawer>
    </>
  );
};

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default UserAdd;
