import React, { useState, useEffect } from 'react';
import ProForm, { ProFormText, ProFormSwitch } from '@ant-design/pro-form';
import { EditOutlined } from '@ant-design/icons';
import { Button, Spin, Drawer, InputNumber, message } from 'antd';
import SelectIcon from '@/pages/system/menu/components/SelectIcon';
import { getMenuById, menuEdit } from '@/services/system/menu';
import MenuParentSelect from '@/pages/system/menu/components/MenuParentSelect';

// 定义传参
interface MenuEditParams {
  refresh: () => void;
  id: string;
}

/**
 * 编辑菜单
 *
 * @author zhangby
 * @date 23/9/20 1:32 pm
 */
const MenuEdit: React.FC<MenuEditParams> = (props) => {
  const { id, children } = props;
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [icon, setIcon] = useState<string>('');
  const [sort, setSort] = useState<number>(10);
  const [parentId, setParentId] = useState<string>('');

  const [form] = ProForm.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    setIcon('');
    // 设置最大排序值
    try {
      setLoading(true);
      const data = await getMenuById(id);
      if (data.code === '000') {
        setSort(data.result.sort);
        setIcon(data.result.icon);
        setParentId(data.result.parentId);
        form.setFieldsValue({ ...data.result, isShowBool: data.result?.isShow === '1' });
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    form.validateFields().then(async (values) => {
      try {
        setLoading(true);
        const data = await menuEdit({
          ...values,
          id,
          icon,
          parentId,
          sort,
          isShow: values.isShowBool ? '1' : '0',
        });
        if (data.code === '000') {
          setLoading(false);
          setVisible(false);
          message.success('编辑菜单成功！');
          props.refresh();
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <span style={{ cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary" shape="circle">
            <EditOutlined />
          </Button>
        )}
      </span>
      {/* 弹出窗口 */}
      <Drawer
        title="编辑菜单"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <ProForm submitter={false} form={form}>
            {parentId !== '0' ? (
              <div>
                <div style={{ marginBottom: 10 }}>上级菜单</div>
                <MenuParentSelect
                  value={parentId}
                  onChange={setParentId}
                  style={{ maxWidth: 420, marginBottom: 10 }}
                />
              </div>
            ) : null}
            <ProFormText
              name="name"
              label="菜单名称"
              placeholder="菜单名称"
              width={400}
              rules={[{ required: true, message: '菜单名称不能为空' }]}
            />
            <ProFormText
              name="href"
              label="链接"
              placeholder="链接"
              width={400}
              rules={[{ required: true, message: '链接不能为空' }]}
            />
            <div style={{ marginBottom: 10 }}>是否显示</div>
            <SelectIcon
              key={icon}
              value={icon}
              onChange={setIcon}
              style={{ width: 400, marginBottom: 20 }}
            />
            <ProFormSwitch name="isShowBool" label="是否显示" width={30} initialValue={true} />
            <div style={{ marginBottom: 10 }}>是否显示</div>
            <InputNumber
              min={0}
              step={1}
              precision={0}
              style={{ marginBottom: 20 }}
              defaultValue={10}
              value={sort}
              onChange={setSort}
            />
            <ProFormText name="target" label="目标路由" placeholder="目标路由" width={400} />
            <ProFormText name="permission" label="授权标识" placeholder="授权标识" width={400} />
          </ProForm>
        </Spin>
      </Drawer>
    </>
  );
};

export default MenuEdit;
