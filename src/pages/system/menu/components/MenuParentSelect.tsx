import React, { useState, useEffect } from 'react';
import { Input, Button, Modal, Tree, message } from 'antd';
import { getMenuList4Data, getMenuById } from '@/services/system/menu';
import SysIcon from '@/pages/common/SysIcon';

const { TreeNode } = Tree;
const { Search } = Input;

// 定义传参
interface MenuParentSelectParams {
  value?: string;
  onChange?: (value: string) => void;
  style?: any;
}

/**
 * 上级菜单
 * mc
 */
const MenuParentSelect: React.FC<MenuParentSelectParams> = (props) => {
  const { value, onChange, style } = props;
  const [text, setText] = useState('');
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selectKey, setSelectKey] = useState('');
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [menuList, setMenuList] = useState([]);

  // 加载
  useEffect(() => {
    if (value && value !== '0') {
      // 查询菜单
      setLoading(true);
      getMenuById(value)
        .then((res) => {
          if (res.code === '000') {
            setSelectKey(value);
            setText(res.result.name);
            setExpandedKeys([res.result.parentId]);
          }
          setLoading(false);
        })
        .catch(() => setLoading(false));
    }
  }, [value]);

  // 初始化
  const onPreInit = () => {
    setVisible(true);
    // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
    queryMenuData({});
  };

  // 查询列表
  const queryMenuData = async (parms: any) => {
    // 查询菜单
    setLoading(true);
    try {
      const { code, result } = await getMenuList4Data(parms);
      if (code === '000') {
        // @ts-ignore
        setMenuList(result);
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    if (selectKey) {
      getMenuById(selectKey).then((res) => {
        if (res.code === '000') {
          setText(res.result.name);
          setExpandedKeys([res.result.parentId]);
          setVisible(false);
          if (onChange) {
            onChange(selectKey);
          }
        }
      });
    } else {
      message.error('请选择上级菜单');
    }
  };

  // 选择
  const onSelect = (keys: string[]) => {
    setSelectKey(keys[0]);
  };

  const renderTreeNodes = (data) =>
    data.map((item) => {
      if (item.children) {
        return (
          <TreeNode
            title={
              <div>
                <SysIcon type={item.icon} style={{ marginRight: 5 }} />
                <span>{item.name}</span>
              </div>
            }
            key={item.id}
            dataRef={item}
          >
            {renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      // eslint-disable-next-line max-len
      return (
        <TreeNode
          title={
            <div>
              <SysIcon type={item.icon} style={{ marginRight: 5 }} />
              <span>{item.name}</span>
            </div>
          }
          key={item.id}
        />
      );
    });

  // 返回
  return (
    <>
      <Input.Group compact style={style}>
        <Input style={{ width: '75%' }} placeholder="上级菜单" value={text} />
        <Button style={{ width: '20%' }} type="primary" onClick={onPreInit}>
          选择
        </Button>
      </Input.Group>
      {/* 弹窗 */}
      <Modal
        title="上级菜单"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={400}
      >
        <div style={{ textAlign: 'center', marginBottom: 20, color: '#5f5f5f' }}>
          <span>关键字：</span>
          <Search
            placeholder="搜索"
            onSearch={(keys) => queryMenuData({ keyword: keys })}
            style={{ width: 240 }}
          />
        </div>
        <Tree
          key={loading}
          style={{ marginLeft: 50 }}
          expandedKeys={expandedKeys}
          onExpand={(keys) => setExpandedKeys(keys)}
          selectedKeys={[selectKey]}
          onSelect={onSelect}
        >
          {renderTreeNodes(menuList)}
        </Tree>
      </Modal>
    </>
  );
};

export default MenuParentSelect;
