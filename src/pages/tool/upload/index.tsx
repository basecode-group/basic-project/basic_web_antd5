import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import UploadUrlConfig from './components/UploadUrlConfig';
import UploadImage from '@/pages/tool/upload/components/UploadImage';
import { getDictList4Type } from '@/services/system/dict';
import UploadVideo from './components/UploadVideo';
import UploadOtherFile from './components/UploadOtherFile';

/**
 * 文件上传管理
 *
 * @author zhangby
 * @date 20/10/20 11:19 am
 */
export default (): React.ReactNode => {
  const [tabKey, setTabKey] = useState<string>('1');
  const [visitUrl, setVisitUrl] = useState<string>('http://localhost:8080/upload');
  const [uploadImgUrl, setUploadImgUrl] = useState<string>(
    'http://localhost:8080/file/upload/image',
  );
  const [uploadVideoUrl, setUploadVideoUrl] = useState<string>(
    'http://localhost:8080/file/upload/video',
  );
  const [uploadOtherUrl, setUploadOtherUrl] = useState<string>(
    'http://localhost:8080/file/upload/other/file',
  );

  // 初始化
  useEffect(() => {
    // 查询访问地址
    getDictList4Type('upload_config').then((res) => {
      if (res.code === '000') {
        const data = res.result;
        // @ts-ignore
        setVisitUrl(data?.filter((item) => item.label === 'upload_visit_url')[0].value);
        // @ts-ignore
        setUploadImgUrl(data?.filter((item) => item.label === 'upload_img_url')[0].value);
        // @ts-ignore
        setUploadVideoUrl(data?.filter((item) => item.label === 'upload_video_url')[0].value);
        // @ts-ignore
        setUploadOtherUrl(data?.filter((item) => item.label === 'upload_other_url')[0].value);
      }
    });
  }, []);

  return (
    <>
      <PageContainer
        subTitle="支持在线上传与文件管理。"
        extra={[<UploadUrlConfig key="urlConfig" />]}
        tabList={[
          {
            tab: '上传图片',
            key: '1',
          },
          {
            tab: '上传视频',
            key: '2',
          },
          {
            tab: '其他文件',
            key: '3',
          },
        ]}
        tabActiveKey={tabKey}
        onTabChange={(keyVal) => setTabKey(keyVal)}
      >
        {tabKey === '1' ? (
          <UploadImage visitUrl={visitUrl} uploadUrl={uploadImgUrl} />
        ) : tabKey === '2' ? (
          <UploadVideo visitUrl={visitUrl} uploadUrl={uploadVideoUrl} />
        ) : (
          <UploadOtherFile visitUrl={visitUrl} uploadUrl={uploadOtherUrl} />
        )}
      </PageContainer>
    </>
  );
};
