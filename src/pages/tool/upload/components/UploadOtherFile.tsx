import React, { useState, useEffect, useRef, ReactText } from 'react';
import UploadPathTree from '@/pages/tool/upload/components/upload/UploadPathTree';
import ProCard from '@ant-design/pro-card';
import { Button, Input, Popconfirm, message } from 'antd';
import { ReloadOutlined, DeleteOutlined, CopyOutlined } from '@ant-design/icons';
import UploadFile4Other from './upload/UploadFile4Other';
import ProTable, { ActionType } from '@ant-design/pro-table';
import AntIcon from '@/pages/common/AntIcon';
import SysText from '@/pages/common/SysText';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { delFile, getUploadFileList } from '@/services/tool/upload';
import FileTypeView from './upload/FileTypeView';

const { Search } = Input;

// 定义传参
interface UploadOtherFileParams {
  visitUrl: string;
  uploadUrl: string;
}

/**
 * 文件上传服务
 *
 * @author zhangby
 * @date 21/10/20 3:34 pm
 */
const UploadOtherFile: React.FC<UploadOtherFileParams> = (props) => {
  const { visitUrl, uploadUrl } = props;
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState<ReactText[]>([]);
  const [query, setQuery] = useState<any>({
    path: '/other',
  });

  const ref = useRef<ActionType>();

  // 删除
  const onDelete = async (id: string) => {
    try {
      setLoading(true);
      const delResult = await delFile(id);
      if (delResult.code === '000') {
        message.success('删除记录成功！');
        // @ts-ignore
        ref.current?.reloadAndRest();
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 批量删除
  const onBatchDelete = () => {
    if (rows.length === 0) {
      message.error('请选择要操作的记录');
      return;
    }
    onDelete(rows.join(','));
  };

  // 搜索
  const onSearch = (val: any) => {
    setQuery({ ...query, ...val });
    ref.current?.reload();
  };

  // 父类调用子类
  const childRef = useRef();

  return (
    <>
      <ProCard split="vertical">
        <ProCard
          title="文件路径"
          colSpan="250px"
          extra={[
            <span key="refresh">
              <Button
                type="link"
                icon={<ReloadOutlined />}
                style={{ color: '#353535', marginTop: -5, padding: 0 }}
                onClick={() => childRef.current?.refresh()}
              >
                刷新
              </Button>
            </span>,
          ]}
        >
          <UploadPathTree
            key="upload"
            ref={childRef}
            pathKey="/other"
            pathName="other"
            refresh={onSearch}
          />
        </ProCard>
        <ProCard
          title="文件列表"
          style={{ paddingTop: 0 }}
          extra={[
            <span key="del">
              {rows.length > 0 ? (
                <Popconfirm
                  title="确定要删除选中的记录吗?"
                  onConfirm={onBatchDelete}
                  placement="topRight"
                  okText="Yes"
                  cancelText="No"
                >
                  <Button type="primary" danger style={{ marginRight: 10 }}>
                    <DeleteOutlined />
                    批量删除
                  </Button>
                </Popconfirm>
              ) : null}
            </span>,
            <UploadFile4Other
              refresh={() => onSearch({})}
              uploadUrl={uploadUrl}
              visitUrl={visitUrl}
            />,
          ]}
        >
          {/* 列表 */}
          <ProTable<any>
            actionRef={ref}
            search={false}
            loading={loading}
            size="small"
            columns={[
              {
                title: '预览',
                dataIndex: 'name',
                align: 'center',
                width: 150,
                render: (item, _) => <FileTypeView type={item.substr(item.lastIndexOf('.') + 1)} />,
              },
              {
                title: '名称',
                dataIndex: 'name',
                width: 150,
                render: (item, record) => (
                  <Button
                    type="link"
                    onClick={() => window.open(visitUrl + record.path)}
                    style={{ margin: 0, padding: 0, fontSize: 12 }}
                  >
                    {item.substr(0, item.lastIndexOf('.'))}
                  </Button>
                ),
              },
              {
                title: '类型',
                dataIndex: 'name',
                align: 'center',
                width: 150,
                render: (item, _) => <SysText>{item.substr(item.lastIndexOf('.') + 1)}</SysText>,
              },
              {
                title: '操作',
                dataIndex: 'path',
                align: 'center',
                width: 200,
                render: (_, record) => (
                  <>
                    <CopyToClipboard
                      text={visitUrl + record.path}
                      onCopy={() => message.success('复制成功')}
                    >
                      <Button type="primary" shape="circle" icon={<CopyOutlined />} />
                    </CopyToClipboard>
                    <Popconfirm
                      placement="topRight"
                      title="确定要删除选中的记录吗?"
                      onConfirm={() => onDelete(record.path)}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button type="primary" shape="circle" danger style={{ marginLeft: 10 }}>
                        <DeleteOutlined />
                      </Button>
                    </Popconfirm>
                  </>
                ),
              },
            ]}
            rowSelection={{
              columnWidth: 20,
              onChange: (selectedRowKeys, _) => setRows(selectedRowKeys),
            }}
            request={(params, sorter, filter) =>
              getUploadFileList({ ...{ ...params, ...query }, sorter, filter })
            }
            tableAlertRender={false}
            pagination={{
              pageSize: 10,
            }}
            rowKey="path"
            headerTitle={
              <Search
                style={{ maxWidth: 300, float: 'left', marginLeft: -24 }}
                placeholder="模糊搜索"
                onSearch={(val) => onSearch({ keyword: val })}
                enterButton
              />
            }
          />
        </ProCard>
      </ProCard>
    </>
  );
};

export default UploadOtherFile;
