import React, { useState } from 'react';
import { Button, Drawer, Spin, message } from 'antd';
import { SettingOutlined } from '@ant-design/icons';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { getDictList4Type } from '@/services/system/dict';
import { uploadConfigUpdate } from '@/services/tool/upload';

// 定义传参
interface UploadUrlConfigParams {
  // refresh: () => void;
}

/**
 * 文件上传地址设置
 *
 * @author zhangby
 * @date 20/10/20 11:35 am
 */
const UploadUrlConfig: React.FC<UploadUrlConfigParams> = (props) => {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const [form] = ProForm.useForm();

  // 初始化
  const onPreInit = () => {
    setVisible(true);
    form.resetFields();
    setLoading(true);
    // 查询
    getDictList4Type('upload_config')
      .then((res) => {
        if (res.code === '000') {
          const data = res.result;
          form.setFieldsValue({
            uploadImgUrl: data?.filter((item) => item.label === 'upload_img_url')[0].value,
            uploadVideoUrl: data?.filter((item) => item.label === 'upload_video_url')[0].value,
            uploadOtherUrl: data?.filter((item) => item.label === 'upload_other_url')[0].value,
            visitUrl: data?.filter((item) => item.label === 'upload_visit_url')[0].value,
            fileSrc: data?.filter((item) => item.label === 'upload_file_src')[0].value,
          });
        }
        setLoading(false);
      })
      .catch(() => setLoading(false));
  };

  // 提交
  const onSubmit = () => {
    form.validateFields().then(async (values) => {
      try {
        const result = await uploadConfigUpdate(values);
        if (result.code === '000') {
          message.success('文件上传设置成功');
          setVisible(false);
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <Button type="primary" icon={<SettingOutlined />} onClick={onPreInit}>
        上传设置
      </Button>
      {/* 弹出窗口 */}
      <Drawer
        title="上传设置"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <ProForm submitter={false} form={form}>
            <ProFormText
              width="l"
              name="fileSrc"
              label="上传路径"
              placeholder="文件上传路径"
              initialValue="classpath:/static/upload"
              rules={[{ required: true, message: '请输入文件上传路径' }]}
            />
            <ProFormText
              width="l"
              name="visitUrl"
              label="访问地址"
              placeholder="访问地址"
              rules={[{ required: true, message: '请输入访问地址' }]}
            />
            <ProFormText
              width="l"
              name="uploadImgUrl"
              label="图片上传地址"
              placeholder="图片上传地址"
              rules={[{ required: true, message: '请输入图片上传地址' }]}
            />
            <ProFormText
              width="l"
              name="uploadVideoUrl"
              label="视频上传地址"
              placeholder="视频上传地址"
              rules={[{ required: true, message: '请输入视频上传地址' }]}
            />
            <ProFormText
              width="l"
              name="uploadOtherUrl"
              label="文件上传地址"
              placeholder="文件上传地址"
              rules={[{ required: true, message: '请输入文件上传地址' }]}
            />
          </ProForm>
        </Spin>
      </Drawer>
    </>
  );
};

export default UploadUrlConfig;
