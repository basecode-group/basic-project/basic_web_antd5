import React, { useState, useEffect } from 'react';
import { Button, Modal, message } from 'antd';
import { UploadOutlined, InboxOutlined } from '@ant-design/icons';
import Dragger from 'antd/lib/upload/Dragger';
import copy from 'copy-to-clipboard';

// 定义传参
interface UploadFile4VideoParams {
  refresh?: () => void;
  visitUrl: string;
  uploadUrl: string;
}

/**
 * 上传图片文件
 *
 * @author zhangby
 * @date 21/10/20 11:48 am
 */
const UploadFile4Video: React.FC<UploadFile4VideoParams> = (props) => {
  const { visitUrl, uploadUrl, refresh } = props;
  const [visible, setVisible] = useState(false);
  const [fileList, setFileList] = useState<any[]>([]);

  // 初始化
  const onPreInit = () => {
    setVisible(true);
    setFileList([]);
  };

  const uploadProps = {
    accept: 'video/*',
    headers: {
      Authorization: localStorage.getItem('token'),
    },
    name: 'file',
    multiple: true,
    listType: 'picture',
    fileList,
    action: uploadUrl,
    onChange(info: any) {
      const { status } = info.file;
      if (status === 'done') {
        // 获取上传地址
        const { response } = info.file;
        if (response !== undefined) {
          if (response.code !== '000') {
            info.fileList.forEach((file: any) => {
              file.status = 'error';
            });
            setFileList(info.fileList);
            message.error(`${info.file.name} 文件上传失败，${response.msg}`);
          } else {
            setFileList(info.fileList);
            message.success(`${info.file.name} 文件上传成功.`);
            // 刷新
            if (refresh) {
              refresh();
            }
          }
        } else {
          message.error(`${info.file.name} 文件上传失败.`);
        }
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
      setFileList(info.fileList);
    },
    showUploadList: {
      showRemoveIcon: true,
    },
    onPreview: (file: any) => {
      const { response } = file;
      if (response.code === '000') {
        copy(`${visitUrl}${response.result.url}`);
        message.success('文件地址复制成功');
      } else {
        message.error('文件复制失败！');
      }
    },
  };

  return (
    <>
      <Button type="primary" icon={<UploadOutlined />} onClick={onPreInit}>
        上传视频
      </Button>

      <Modal
        title="上传视频"
        visible={visible}
        onCancel={() => setVisible(false)}
        width={600}
        footer={[
          <Button key="submit" onClick={() => setVisible(false)}>
            关闭
          </Button>,
        ]}
      >
        <Dragger {...uploadProps} style={{ marginBottom: 20 }}>
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">单击或拖动文件到该区域以上传</p>
          <p className="ant-upload-hint">支持单次或批量上传。严格禁止上传公司数据或其他乐队文件</p>
        </Dragger>
      </Modal>
    </>
  );
};

export default UploadFile4Video;
