import React, { useState, useEffect } from 'react';
import AntIcon from '@/pages/common/AntIcon';

import { FileIcon, defaultStyles } from 'react-file-icon';

// 定义传参
interface FileTypeViewParams {
  type?: string;
}

/**
 * 文件类型预览
 *
 * @author zhangby
 * @date 21/10/20 4:44 pm
 */
const FileTypeView: React.FC<FileTypeViewParams> = (props) => {
  const { type } = props;

  // 图片格式 转小写
  const imgType = 'BMP、DIB、PCP、DIF、WMF、GIF、JPG、TIF、EPS、PSD、CDR、IFF、TGA、PCD、MPT、PNG'.toLowerCase();
  // 视频格式 转小写
  const videoType = 'AVI、mov、rmvb、rm、FLV、mp4、3GP、wmv、mpeg、mkv、asf'.toLowerCase();
  // 视频格式 转小写
  const songType = 'CD、OGG、MP3、ASF、WMA、WAV、MP3PRO；RM、REAL、APE、MODULE、MIDI、VQF'.toLowerCase();
  // Xml 转小写
  const xmlType = 'xml'.toLowerCase();
  // Html 转小写
  const htmlType = 'html、htm'.toLowerCase();
  // Html 转小写
  const xlsType = 'xlsx、xls、csv'.toLowerCase();
  // 文档 转小写
  const wordType = 'doc、docx、dot、dotx、docm、dotm'.toLowerCase();

  return (
    <div style={{ margin: '0px auto', width: 25 }}>
      {type && imgType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-image" style={{ fontSize: '1.6em' }} />
      ) : type && videoType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-video" style={{ fontSize: '1.6em' }} />
      ) : type && songType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-audio" style={{ fontSize: '1.6em' }} />
      ) : type && xmlType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-xml" style={{ fontSize: '1.6em' }} />
      ) : type && htmlType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-html" style={{ fontSize: '1.6em' }} />
      ) : type && xlsType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-excel" style={{ fontSize: '1.6em' }} />
      ) : type && wordType.indexOf(type.toLowerCase()) >= 0 ? (
        <AntIcon type="icon-word" style={{ fontSize: '1.6em' }} />
      ) : (
        <AntIcon type="icon-unknown" style={{ fontSize: '1.6em' }} />
      )}
    </div>
  );
};

export default FileTypeView;
