import React, { useState, useEffect, useImperativeHandle, forwardRef } from 'react';
import { Spin, Tree } from 'antd';
import { getUploadPath, getUploadPathInit } from '@/services/tool/upload';

// 定义传参
interface UploadPathTreeParams {
  refresh?: (params: any) => void;
  pathName: string;
  pathKey: string;
}

/**
 * 视频路径Tree
 *
 * @author zhangby
 * @date 21/10/20 2:43 pm
 */
const UploadPathTree: React.FC<UploadPathTreeParams> = (props, ref) => {
  const { pathName, pathKey, refresh } = props;
  const [loading, setLoading] = useState(false);
  const [pathData, setPathData] = useState([]);
  const [defExpKey, setDefExpKey] = useState<string[]>([pathKey]);
  const [selKey, setSelKey] = useState<string[]>([pathKey]);

  // 初始化
  useEffect(() => {
    if (pathKey) {
      // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
      refreshData();
    }
  }, [pathKey]);

  // 刷新
  const refreshData = () => {
    setLoading(true);
    // @ts-ignore
    getUploadPathInit(pathKey)
      .then((res) => {
        if (res.code === '000') {
          setPathData([
            {
              title: pathName,
              key: pathKey,
              children: res.result,
            },
          ]);
          // 设置key
          setDefExpKey([...defExpKey, res.result[0]?.key]);
        }
        setLoading(false);
      })
      .catch(() => setLoading(false));
  };

  // 父类调用
  useImperativeHandle(ref, () => ({
    // changeVal 就是暴露给父组件的方法
    refresh: async () => {
      // 提交
      refreshData();
    },
  }));

  // 加载
  const onLoadData = (treeNode: any) =>
    new Promise((resolve) => {
      if (treeNode.props.children) {
        resolve();
        return;
      }
      getUploadPath(treeNode.props.eventKey).then((res) => {
        if (res.code === '000') {
          const data = [];
          res.result.forEach((item) => data.push({ ...item }));
          treeNode.props.data['children'] = data;
          setPathData([...pathData]);
          resolve();
        }
      });
    });

  // 选择tree
  const onSelectTree = (keys: string[]) => {
    if (keys.length > 0) {
      setSelKey(keys);
      if (refresh) {
        refresh({ path: keys[0] });
      }
    } else {
      // @ts-ignore
      setSelKey(pathKey);
      if (refresh) {
        refresh({ path: pathKey });
      }
    }
  };

  // 返回
  return (
    <>
      <Spin spinning={loading}>
        <Tree
          key={loading}
          showLine
          loadData={onLoadData}
          defaultExpandParent
          defaultExpandedKeys={defExpKey}
          treeData={pathData}
          selectedKeys={selKey}
          onSelect={onSelectTree}
        />
      </Spin>
    </>
  );
};

// @ts-ignore
export default forwardRef(UploadPathTree);
