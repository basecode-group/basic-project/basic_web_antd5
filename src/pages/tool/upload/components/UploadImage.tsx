import React, { ReactText, useRef, useState } from 'react';
import ProCard from '@ant-design/pro-card';
import { Avatar, Button, Input, message, Popconfirm } from 'antd';
import { delFile, getUploadFileList } from '@/services/tool/upload';
import ProTable, { ActionType } from '@ant-design/pro-table';
import SysText from '@/pages/common/SysText';
import { CopyOutlined, DeleteOutlined, ReloadOutlined } from '@ant-design/icons';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import UploadFile4Img from './upload/UploadFile4Img';
import UploadPathTree from '@/pages/tool/upload/components/upload/UploadPathTree';

const { Search } = Input;

// 定义传参
interface UploadImageParams {
  visitUrl: string;
  uploadUrl: string;
}

/**
 * 上传图片
 *
 * @author zhangby
 * @date 20/10/20 2:55 pm
 */
const UploadImage: React.FC<UploadImageParams> = (props) => {
  const { visitUrl, uploadUrl } = props;
  const [tableLoading, setTableLoading] = useState<boolean>(false);
  const [rows, setRows] = useState<ReactText[]>([]);
  const [query, setQuery] = useState<any>({
    path: '/image',
  });

  const ref = useRef<ActionType>();

  // 删除
  const onDelete = async (id: string) => {
    try {
      setTableLoading(true);
      const delResult = await delFile(id);
      if (delResult.code === '000') {
        message.success('删除记录成功！');
        // @ts-ignore
        ref.current?.reloadAndRest();
      }
      setTableLoading(false);
    } catch (e) {
      setTableLoading(false);
    }
  };

  // 批量删除
  const onBatchDelete = () => {
    if (rows.length === 0) {
      message.error('请选择要操作的记录');
      return;
    }
    onDelete(rows.join(','));
  };

  // 搜索
  const onSearch = (val: any) => {
    setQuery({ ...query, ...val });
    ref.current?.reload();
  };

  // 父类调用子类
  const childRef = useRef();

  return (
    <>
      <ProCard split="vertical">
        <ProCard
          title="图片路径"
          colSpan="250px"
          extra={[
            <span key="refresh">
              <Button
                type="link"
                icon={<ReloadOutlined />}
                style={{ color: '#353535', marginTop: -5, padding: 0 }}
                onClick={() => childRef.current?.refresh()}
              >
                刷新
              </Button>
            </span>,
          ]}
        >
          <UploadPathTree ref={childRef} pathKey="/image" pathName="image" refresh={onSearch} />
        </ProCard>
        <ProCard
          title="图片列表"
          style={{ paddingTop: 0 }}
          extra={[
            <span key="del">
              {rows.length > 0 ? (
                <Popconfirm
                  title="确定要删除选中的记录吗?"
                  onConfirm={onBatchDelete}
                  placement="topRight"
                  okText="Yes"
                  cancelText="No"
                >
                  <Button type="primary" danger style={{ marginRight: 10 }}>
                    <DeleteOutlined />
                    批量删除
                  </Button>
                </Popconfirm>
              ) : null}
            </span>,
            <UploadFile4Img
              key="upload"
              uploadUrl={uploadUrl}
              visitUrl={visitUrl}
              refresh={() => onSearch({})}
            />,
          ]}
        >
          {/* 列表 */}
          <ProTable<any>
            actionRef={ref}
            search={false}
            loading={tableLoading}
            size="small"
            columns={[
              {
                title: '预览',
                dataIndex: 'path',
                align: 'center',
                width: 150,
                render: (item, _) => <Avatar size={35} src={visitUrl + item} />,
              },
              {
                title: '名称',
                dataIndex: 'name',
                width: 150,
                render: (item, record) => (
                  <Button
                    type="link"
                    onClick={() => window.open(visitUrl + record.path)}
                    style={{ margin: 0, padding: 0, fontSize: 12 }}
                  >
                    {item.substr(0, item.lastIndexOf('.'))}
                  </Button>
                ),
              },
              {
                title: '类型',
                dataIndex: 'name',
                align: 'center',
                width: 150,
                render: (item, _) => <SysText>{item.substr(item.lastIndexOf('.') + 1)}</SysText>,
              },
              {
                title: '操作',
                dataIndex: 'path',
                align: 'center',
                width: 200,
                render: (_, record) => (
                  <>
                    <CopyToClipboard
                      text={visitUrl + record.path}
                      onCopy={() => message.success('复制成功')}
                    >
                      <Button type="primary" shape="circle" icon={<CopyOutlined />} />
                    </CopyToClipboard>
                    <Popconfirm
                      placement="topRight"
                      title="确定要删除选中的记录吗?"
                      onConfirm={() => onDelete(record.path)}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button type="primary" shape="circle" danger style={{ marginLeft: 10 }}>
                        <DeleteOutlined />
                      </Button>
                    </Popconfirm>
                  </>
                ),
              },
            ]}
            rowSelection={{
              columnWidth: 20,
              onChange: (selectedRowKeys, _) => setRows(selectedRowKeys),
            }}
            request={(params, sorter, filter) =>
              getUploadFileList({ ...{ ...params, ...query }, sorter, filter })
            }
            tableAlertRender={false}
            pagination={{
              pageSize: 10,
            }}
            rowKey="path"
            headerTitle={
              <Search
                style={{ maxWidth: 300, float: 'left', marginLeft: -24 }}
                placeholder="模糊搜索"
                onSearch={(val) => onSearch({ keyword: val })}
                enterButton
              />
            }
          />
        </ProCard>
      </ProCard>
    </>
  );
};

export default UploadImage;
