import React, { useRef, useState } from 'react';
import { getSysJobs } from '@/services/tool/cron';
import ProTable, { ActionType } from '@ant-design/pro-table';
import CronConfig from '@/pages/common/cron/CronConfig';
import { Badge, Button, Input } from 'antd';
import CronEdit from '@/pages/common/cron/CronEdit';
import SysText from '@/pages/common/SysText';

const { Search } = Input;

// 定义传参
interface CronSystemListParams {}

/**
 * 系统定时任务
 *
 * @author zhangby
 * @date 16/10/20 12:05 pm
 */
const CronSystemList: React.FC<CronSystemListParams> = (props) => {
  const [query, setQuery] = useState<string>('');

  const ref = useRef<ActionType>();

  // 搜索
  const onSearch = (val?: string) => {
    // @ts-ignore
    setQuery(val);
    ref.current?.reload();
  };

  return (
    <>
      {/* 列表 */}
      <ProTable<Tool.SysCron>
        actionRef={ref}
        search={false}
        columns={[
          {
            title: '名称',
            dataIndex: 'cronConfig',
            width: 150,
            render: (_, record) => (
              <CronConfig cronId={record?.dbCronId}>
                {record?.cron !== record?.dbCron ? (
                  <span style={{ color: '#eb2f96', cursor: 'pointer' }}>
                    {record?.cronConfig?.description}
                  </span>
                ) : (
                  <Button style={{ marginLeft: 0, padding: 0 }} type="link">
                    {record?.cronConfig?.description}
                  </Button>
                )}
              </CronConfig>
            ),
          },
          {
            title: '标识',
            dataIndex: 'mark',
            width: 150,
            render: (item, _) => <SysText>{item}</SysText>,
          },
          {
            title: '当前定时任务',
            dataIndex: 'cron',
            width: 150,
            render: (item, record) => (
              <span>
                {item !== record?.dbCron ? (
                  <CronEdit value={item} isEdit={false}>
                    <span style={{ color: '#eb2f96' }}>{item}</span>
                  </CronEdit>
                ) : (
                  <CronEdit value={item} isEdit={false} />
                )}
              </span>
            ),
          },
          {
            title: '状态',
            dataIndex: 'status',
            width: 150,
            render: (item, record) => (
              <span>
                {item?.value === 'NORMAL' ? (
                  <span style={{ color: '#108ee9' }}>
                    <Badge status="processing" />
                    {item?.label}
                  </span>
                ) : (
                  <span style={{ color: '#f50' }}>
                    <Badge status="error" />
                    {item?.label}
                  </span>
                )}
              </span>
            ),
          },
        ]}
        request={(params, sorter, filter) =>
          getSysJobs({ ...{ ...params, keyword: query }, sorter, filter })
        }
        pagination={{
          pageSize: 10,
        }}
        rowKey="mark"
        headerTitle="系统定时任务列表"
        toolBarRender={() => [
          <Search placeholder="搜索" onSearch={onSearch} style={{ minWidth: 400 }} enterButton />,
        ]}
      />
    </>
  );
};

export default CronSystemList;
