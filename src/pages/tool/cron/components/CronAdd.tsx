import React, { useState, useEffect } from 'react';
import { Button, Drawer, Spin, Form, Input, Select, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getCronSelectData, saveCron } from '@/services/tool/cron';
import SelectCron from '@/pages/common/cron/SelectCron';

const { Option } = Select;
const { TextArea } = Input;

// 定义传参
interface CronAddParams {
  refresh?: () => void;
}

/**
 * 新建定时任务
 *
 * @author zhangby
 * @date 15/10/20 2:02 pm
 */
const CronAdd: React.FC<CronAddParams> = (props) => {
  const { children, refresh } = props;
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [cronSelectData, setCronSelectData] = useState<API.DictMap[]>([]);

  const [form] = Form.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    // 查询下拉框数据
    try {
      setLoading(true);
      const cronSelectDataResult = await getCronSelectData();
      setLoading(false);
      if (cronSelectDataResult.code === '000') {
        // @ts-ignore
        setCronSelectData(cronSelectDataResult.result);
      }
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    form.validateFields().then(async (values) => {
      try {
        setLoading(true);
        const cronResult = await saveCron(values);
        if (cronResult.code === '000') {
          message.success('新建定时任务成功');
          setVisible(false);
          if (refresh) {
            refresh();
          }
        }
        setLoading(false);
      } catch (e) {
        setLoading(false);
      }
    });
  };

  // 选择Cron
  const selectCron = (_, item: any) => {
    if (item) {
      const { value, cron } = item;
      const rk = item.children.replace('（ ', '').replace(' ）', '').replace(value, '').trim();
      // 设置
      form.setFieldsValue({
        name: rk,
        remarks: rk,
        cron,
      });
    }
  };

  return (
    <>
      <span style={{ marginLeft: 10, cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary">
            <PlusOutlined />
            新建代码
          </Button>
        )}
      </span>
      {/* 弹出窗口 */}
      <Drawer
        title="新建定时任务"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <Form {...layout} form={form}>
            <Form.Item
              name="mark"
              label="标识"
              rules={[{ required: true, message: '标识不能为空' }]}
            >
              <Select placeholder="选择定时任务" showSearch allowClear onChange={selectCron}>
                {cronSelectData.map((item, key) => (
                  <Option
                    value={item.value}
                    cron={item.cron}
                    key={key}
                  >{`${item.value} （ ${item.label} ）`}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="name"
              label="名称"
              rules={[{ required: true, message: '名称不能为空' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="cron"
              label="Cron 表达式"
              rules={[{ required: true, message: '请选择Cron 表达式' }]}
              initialValue="* * * * * ?"
            >
              <SelectCron />
            </Form.Item>
            <Form.Item name="remarks" label="备注信息">
              <TextArea placeholder="备注信息" autoSize={{ minRows: 3, maxRows: 5 }} />
            </Form.Item>
          </Form>
        </Spin>
      </Drawer>
    </>
  );
};

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default CronAdd;
