import React, { useState, useEffect, useRef } from 'react';
import ProTable, { ActionType } from '@ant-design/pro-table';
import { getMonitorJobs } from '@/services/tool/cron';
import CronConfig from '@/pages/common/cron/CronConfig';
import CronEdit from '@/pages/common/cron/CronEdit';
import { Input, Button, Badge } from 'antd';
import SysText from '@/pages/common/SysText';

const { Search } = Input;

// 定义传参
interface CronMonitorListParams {}

/**
 * 系统定时任务监控
 *
 * @author zhangby
 * @date 16/10/20 2:34 pm
 */
const CronMonitorList: React.FC<CronMonitorListParams> = (props) => {
  const [query, setQuery] = useState<string>('');

  const ref = useRef<ActionType>();

  // 搜索
  const onSearch = (val?: string) => {
    // @ts-ignore
    setQuery(val);
    ref.current?.reload();
  };
  return (
    <>
      {/* 列表 */}
      <ProTable<Tool.SysCron>
        actionRef={ref}
        search={false}
        columns={[
          {
            title: '名称',
            dataIndex: 'cronConfig',
            width: 150,
            render: (_, record) => (
              <CronConfig cronId={record?.dbCronId}>
                <Button style={{ marginLeft: 0, padding: 0 }} type="link">
                  {record?.cronConfig?.description}
                </Button>
              </CronConfig>
            ),
          },
          {
            title: '标识',
            dataIndex: 'mark',
            width: 150,
            render: (item, _) => <SysText>{item}</SysText>,
          },
          {
            title: '当前定时任务',
            dataIndex: 'cron',
            width: 150,
            render: (item, _) => <CronEdit value={item} isEdit={false} />,
          },
          {
            title: '状态',
            dataIndex: 'status',
            width: 150,
            render: (item, record) => (
              <span>
                {item?.value === 'NORMAL' ? (
                  <span style={{ color: '#108ee9' }}>
                    <Badge status="processing" />
                    {item?.label}
                  </span>
                ) : (
                  <span style={{ color: '#f50' }}>
                    <Badge status="error" />
                    {item?.label}
                  </span>
                )}
              </span>
            ),
          },
        ]}
        request={(params, sorter, filter) =>
          getMonitorJobs({ ...{ ...params, keyword: query }, sorter, filter })
        }
        pagination={{
          pageSize: 10,
        }}
        rowKey="mark"
        headerTitle="定时任务监控列表"
        toolBarRender={() => [
          <Search placeholder="搜索" onSearch={onSearch} style={{ minWidth: 400 }} enterButton />,
        ]}
      />
    </>
  );
};

export default CronMonitorList;
