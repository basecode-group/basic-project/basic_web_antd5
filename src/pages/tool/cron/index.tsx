import React, { useRef, useState, ReactText } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType } from '@ant-design/pro-table';
import { Button, Popconfirm, Spin, Typography, Input, message } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { getCronList, deleteCron } from '@/services/tool/cron';
import SysText from '@/pages/common/SysText';
import CronAdd from './components/CronAdd';
import CronEdit from '@/pages/tool/cron/components/CronEdit';
import StatusText from '@/pages/common/StatusText';
import CronConfig from '@/pages/common/cron/CronConfig';
import CronSystemList from './components/CronSystemList';
import CronMonitorList from './components/CronMonitorList';

const { Text } = Typography;
const { Search } = Input;

/**
 * 定时任务管理
 *
 * @author zhangby
 * @date 15/10/20 12:08 pm
 */
export default (): React.ReactNode => {
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState<ReactText[]>([]);
  const [query, setQuery] = useState<string>('');
  const [keyTab, setKeyTab] = useState<string>('1');

  const ref = useRef<ActionType>();

  // 搜索
  const onSearch = (val?: string) => {
    // @ts-ignore
    setQuery(val);
    ref.current?.reload();
  };

  // 删除记录
  const onDelete = async (id: string) => {
    try {
      setLoading(true);
      const delResult = await deleteCron(id);
      if (delResult.code === '000') {
        // 删除成功
        message.success('定时任务删除成功');
        // @ts-ignore
        ref.current?.reloadAndRest();
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 批量删除
  const onBatchDelete = () => {
    if (rows.length === 0) {
      message.error('请选择要操作的记录');
      return;
    }
    onDelete(rows.join(','));
  };

  return (
    <>
      <PageContainer
        subTitle="代码生成器，是基于 Quartz 来封装的。"
        content={
          <span>
            {keyTab === '1' ? (
              <span>
                使用定时任务控制功能，需要引入<Text type="danger"> bacisic-common </Text>项目的
                <Text type="danger"> cron </Text>模块。
              </span>
            ) : keyTab === '2' ? (
              <span>
                系统定时任务：<Text type="danger"> Quartz </Text> 在内存中，正常运行的任务。
              </span>
            ) : (
              <span>
                定时任务监控：<Text type="danger"> 数据库Cron </Text>与
                <Text type="danger"> Quartz </Text>内存不同时的任务。
              </span>
            )}
          </span>
        }
        extra={[
          <span key="del">
            {rows.length > 0 ? (
              <Popconfirm
                title="确定要删除选中的记录吗?"
                onConfirm={onBatchDelete}
                okText="Yes"
                cancelText="No"
              >
                <Button type="primary" danger>
                  <DeleteOutlined />
                  批量删除
                </Button>
              </Popconfirm>
            ) : null}
          </span>,
          <CronAdd key="add" refresh={() => ref.current?.reload()} />,
        ]}
        tabList={[
          {
            tab: '基础任务管理',
            key: '1',
          },
          {
            tab: '系统定时任务',
            key: '2',
          },
          {
            tab: '定时任务监控',
            key: '3',
          },
        ]}
        tabActiveKey={keyTab}
        onTabChange={(key) => setKeyTab(key)}
      >
        {keyTab === '1' ? (
          <Spin spinning={loading}>
            {/* 列表 */}
            <ProTable<Tool.SysCron>
              actionRef={ref}
              search={false}
              columns={[
                {
                  title: '名称',
                  dataIndex: 'name',
                  width: 150,
                  render: (_, record) => (
                    <span>
                      <CronEdit cronId={record.id} refresh={() => ref.current?.reload()}>
                        <Button style={{ marginLeft: 0, padding: 0 }} type="link">
                          {record.name}
                        </Button>
                      </CronEdit>
                    </span>
                  ),
                },
                {
                  title: '标识',
                  dataIndex: 'mark',
                  width: 150,
                  render: (item, _) => <SysText>{item}</SysText>,
                },
                {
                  title: 'Cron表达式',
                  dataIndex: 'cron',
                  width: 150,
                  render: (item, record) => (
                    <CronConfig cronId={record.id}>
                      <Button style={{ marginLeft: 0, padding: 0 }} type="link">
                        {item}
                        <span
                          style={{
                            color:
                              record.tails?.cronStatus?.value === 'NORMAL' ? '#2091ff' : '#ff5500',
                          }}
                        >
                          （ {record.tails?.cronStatus?.label} ）
                        </span>
                      </Button>
                    </CronConfig>
                  ),
                },
                {
                  title: '状态',
                  dataIndex: 'status',
                  width: 150,
                  render: (item, _) => <StatusText value={item} />,
                },
                {
                  title: '备注',
                  dataIndex: 'remarks',
                  width: 150,
                  render: (item, _) => <SysText>{item}</SysText>,
                },
                {
                  title: '操作',
                  dataIndex: 'option',
                  valueType: 'option',
                  align: 'center',
                  width: 200,
                  render: (_, record) => (
                    <>
                      <CronEdit cronId={record.id} refresh={() => ref.current?.reload()} />
                      <Popconfirm
                        placement="topRight"
                        title="确定要删除选中的记录吗?"
                        onConfirm={() => onDelete(record.id)}
                        okText="Yes"
                        cancelText="No"
                      >
                        <Button type="primary" shape="circle" danger style={{ marginLeft: 10 }}>
                          <DeleteOutlined />
                        </Button>
                      </Popconfirm>
                    </>
                  ),
                },
              ]}
              rowSelection={{
                columnWidth: 20,
                onChange: (selectedRowKeys, _) => setRows(selectedRowKeys),
              }}
              request={(params, sorter, filter) =>
                getCronList({ ...{ ...params, keyword: query }, sorter, filter })
              }
              pagination={{
                pageSize: 10,
              }}
              rowKey="id"
              headerTitle="代码生成列表"
              toolBarRender={() => [
                <Search
                  placeholder="搜索"
                  onSearch={onSearch}
                  style={{ minWidth: 400 }}
                  enterButton
                />,
              ]}
            />
          </Spin>
        ) : keyTab === '2' ? (
          <CronSystemList />
        ) : (
          <CronMonitorList />
        )}
      </PageContainer>
    </>
  );
};
