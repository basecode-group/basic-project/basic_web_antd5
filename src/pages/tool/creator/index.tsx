import React, { ReactText, useRef, useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable, { ActionType } from '@ant-design/pro-table';
import { getCreatorList, deleteCreator, generatorCode } from '@/services/tool/creator';
import { Button, Popconfirm, Tag, Input, message, Spin, Typography } from 'antd';
import { DeleteOutlined, PrinterOutlined } from '@ant-design/icons';
import SysText from '@/pages/common/SysText';
import CreatorAdd from './components/CreatorAdd';
import CreatorEdit from './components/CreatorEdit';

const { Search } = Input;
const { Text } = Typography;

/**
 * 代码生成器
 *
 * @author zhangby
 * @date 14/10/20 5:10 pm
 */
export default (): React.ReactNode => {
  const [rows, setRows] = useState<ReactText[]>([]);
  const ref = useRef<ActionType>();
  const [loading, setLoading] = useState(false);
  const [query, setQuery] = useState<string>('');

  // 删除
  const onDelete = async (id: string) => {
    try {
      setLoading(true);
      const delResult = await deleteCreator(id);
      if (delResult.code === '000') {
        message.success('删除记录成功');
        // @ts-ignore
        ref.current?.reloadAndRest();
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 批量删除
  const onBatchDelete = () => {
    if (rows.length === 0) {
      message.error('请选择要操作的记录');
      return;
    }
    onDelete(rows.join(','));
  };

  // 代码生成
  const onCodeGenerator = async () => {
    try {
      if (rows.length === 0) {
        message.error('请选择要操作的记录');
        return;
      }
      setLoading(true);
      const codeResult = await generatorCode(rows.join(','));
      if (codeResult.code === '000') {
        message.success('代码生成成功');
        setLoading(false);
        ref.current?.clearSelected();
      }
    } catch (e) {
      setLoading(false);
    }
  };

  // 搜索
  const onSearch = (val?: string) => {
    // @ts-ignore
    setQuery(val);
    ref.current?.reload();
  };

  return (
    <>
      <PageContainer
        subTitle="代码生成器，是基于Mybatis-Plus来封装的。"
        content={
          <span>
            使用代码生成器功能，需要引入<Text type="danger"> bacisic-common </Text>项目的
            <Text type="danger"> creator </Text>模块。
          </span>
        }
        extra={[
          <span key="code">
            {rows.length > 0 ? (
              <Popconfirm
                title="确定要生成代码吗?"
                onConfirm={onCodeGenerator}
                okText="Yes"
                cancelText="No"
              >
                <Button danger>
                  <PrinterOutlined />
                  生成代码
                </Button>
              </Popconfirm>
            ) : null}
          </span>,
          <span key="del">
            {rows.length > 0 ? (
              <Popconfirm
                title="确定要删除选中的记录吗?"
                onConfirm={onBatchDelete}
                okText="Yes"
                cancelText="No"
              >
                <Button type="primary" danger>
                  <DeleteOutlined />
                  批量删除
                </Button>
              </Popconfirm>
            ) : null}
          </span>,
          <CreatorAdd key="add" refresh={onSearch} />,
        ]}
      >
        <Spin spinning={loading}>
          {/* 列表 */}
          <ProTable<Tool.SysCreator>
            actionRef={ref}
            search={false}
            columns={[
              {
                title: '标签名',
                dataIndex: 'name',
                width: 150,
                render: (_, record) => (
                  <span>
                    <CreatorEdit creatorId={record.id} refresh={onSearch}>
                      <Button style={{ marginLeft: 0, padding: 0 }} type="link">
                        {record.name}
                      </Button>
                    </CreatorEdit>
                  </span>
                ),
              },
              {
                title: '表名',
                dataIndex: 'tableName',
                width: 150,
              },
              {
                title: '创建人',
                dataIndex: 'author',
                width: 150,
                render: (item, _) => <Tag color="volcano">{item}</Tag>,
              },
              {
                title: '输出路径',
                dataIndex: 'outPutDir',
                width: 150,
                render: (item, _) => <SysText>{item}</SysText>,
              },
              {
                title: '包名',
                dataIndex: 'packageDir',
                width: 150,
                render: (item, _) => <SysText>{item}</SysText>,
              },
              {
                title: '前缀',
                dataIndex: 'tablePrefix',
                width: 100,
                render: (item, _) => <SysText>{item}</SysText>,
              },
              {
                title: '操作',
                dataIndex: 'option',
                valueType: 'option',
                align: 'center',
                width: 200,
                render: (_, record) => (
                  <>
                    <CreatorEdit creatorId={record.id} refresh={onSearch} />
                    <Popconfirm
                      placement="topRight"
                      title="确定要删除选中的记录吗?"
                      onConfirm={() => onDelete(record.id)}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button type="primary" shape="circle" danger style={{ marginLeft: 10 }}>
                        <DeleteOutlined />
                      </Button>
                    </Popconfirm>
                  </>
                ),
              },
            ]}
            rowSelection={{
              columnWidth: 20,
              onChange: (selectedRowKeys, _) => setRows(selectedRowKeys),
            }}
            request={(params, sorter, filter) =>
              getCreatorList({ ...{ ...params, keyword: query }, sorter, filter })
            }
            pagination={{
              pageSize: 10,
            }}
            rowKey="id"
            headerTitle="代码生成列表"
            toolBarRender={() => [
              <Search
                placeholder="搜索"
                onSearch={onSearch}
                style={{ minWidth: 400 }}
                enterButton
              />,
            ]}
          />
        </Spin>
      </PageContainer>
    </>
  );
};
