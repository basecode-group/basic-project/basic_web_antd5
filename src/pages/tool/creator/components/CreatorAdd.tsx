import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Select, TreeSelect, message, Spin } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getCreatorFileTypeData, getTableList, saveCreator } from '@/services/tool/creator';
import InputPath from '@/pages/tool/creator/components/InputPath';

const { Option } = Select;

// 定义传参
interface CreatorAddParams {
  refresh?: () => void;
}

// 内容参数
interface CreatorItemParams {
  fileType: API.DictMap;
  tableList: [
    {
      table: string;
      description: string;
    },
  ];
}

/**
 * 新建代码生成器
 *
 * @author zhangby
 * @date 14/10/20 6:08 pm
 */
const CreatorAdd: React.FC<CreatorAddParams> = (props) => {
  const { children, refresh } = props;
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [itemParams, setItemParams] = useState<CreatorItemParams>();

  const [form] = Form.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    // 查询数据
    setLoading(true);
    try {
      const params = {};
      // 查询文件生成类型
      const fileTypeData = await getCreatorFileTypeData();
      if (fileTypeData.code === '000') {
        params['fileType'] = fileTypeData.result;
      }
      // 查询数据库
      const tableListResult = await getTableList();
      if (tableListResult.code === '000') {
        params['tableList'] = tableListResult.result;
      }
      // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
      setItemParamsMethod(params);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 设置参数
  const setItemParamsMethod = (params: any) => {
    setItemParams({ ...itemParams, ...params });
  };

  // 提交
  const onSubmit = () => {
    form.validateFields().then(async (values) => {
      // console.log(values);
      // setVisible(false);
      try {
        setLoading(true);
        const addResult = await saveCreator({
          ...values,
          tableName: values.tableName.join(','),
          createFile: values.createFile.join(','),
        });
        if (addResult.code === '000') {
          message.success('新建成功！');
          setLoading(false);
          setVisible(false);
          if (refresh) {
            refresh();
          }
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  // @ts-ignore
  return (
    <>
      <span style={{ marginLeft: 10, cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary">
            <PlusOutlined />
            新建代码
          </Button>
        )}
      </span>
      {/* 弹出窗口 */}
      <Modal
        title="新建代码生成器"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={600}
      >
        <Spin spinning={loading}>
          <Form {...layout} form={form}>
            <Form.Item
              name="name"
              label="标签名"
              rules={[{ required: true, message: '标签名不能为空' }]}
            >
              <Input placeholder="标签名" />
            </Form.Item>
            <Form.Item
              name="author"
              label="操作人"
              rules={[{ required: true, message: '操作人不能为空' }]}
            >
              <Input placeholder="操作人" />
            </Form.Item>
            <Form.Item
              name="tableName"
              label="数据库表"
              rules={[{ required: true, message: '数据库表不能为空' }]}
            >
              <Select mode="multiple" placeholder="数据库表" allowClear>
                {itemParams &&
                  itemParams.tableList?.map((item, key) => (
                    <Option value={item.table} key={key}>
                      {item.table} （ {item.description} ）
                    </Option>
                  ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="createFile"
              label="生成文件"
              rules={[{ required: true, message: '生成文件不能为空' }]}
              initialValue={['ENTITY', 'CONTROLLER', 'SERVICE', 'SERVICE_IMPL', 'MAPPER', 'OTHER']}
            >
              <TreeSelect
                allowClear
                placeholder="生成文件"
                treeCheckable
                treeDefaultExpandedKeys={['0']}
                treeDefaultExpandAll
                dropdownStyle={{ maxHeight: 350 }}
                treeData={itemParams && itemParams.fileType?.children}
              />
            </Form.Item>
            <Form.Item
              name="outPutDir"
              label="文件输出路径"
              rules={[{ required: true, message: '文件输出路径不能为空' }]}
              initialValue="/src/main/java"
            >
              <InputPath />
            </Form.Item>
            <div
              style={{
                position: 'relative',
                top: -20,
                marginLeft: 160,
                fontSize: 12,
                color: '#9E9E9E',
              }}
            >
              文件输出路径，如需更改请填写，否则默认为空
            </div>
            <Form.Item
              name="packageDir"
              label="包名"
              rules={[{ required: true, message: '包名不能为空' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="baseEntity"
              label="基础类"
              initialValue="com.benson.common.common.entity.BaseEntity"
            >
              <Input />
            </Form.Item>
            <Form.Item name="tablePrefix" label="前缀">
              <Input />
            </Form.Item>
            <div
              style={{
                position: 'relative',
                top: -20,
                marginLeft: 160,
                fontSize: 12,
                color: '#9E9E9E',
              }}
            >
              多个前缀用逗号间隔：sys_{' '}
            </div>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default CreatorAdd;
