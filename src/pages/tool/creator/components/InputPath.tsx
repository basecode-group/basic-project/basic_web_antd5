import React, { useState, useEffect } from 'react';
import { Input } from 'antd';

// 定义传参
interface InputPathParams {
  value?: string;
  onChange?: (value: string) => void;
}

/**
 * 路径文本
 *
 * @author zhangby
 * @date 15/10/20 10:29 am
 */
const InputPath: React.FC<InputPathParams> = (props) => {
  const { value, onChange } = props;
  const [firstPath, setFirstPath] = useState<string>('');
  const [secondPath, setSecondPath] = useState<string>('');

  // 初始化
  useEffect(() => {
    if (value) {
      const paths = value.split('/src/main/java');
      setFirstPath(paths[0]);
      setSecondPath(`/src/main/java${paths[1]}`);
    }
  }, [value]);

  // 改变值
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const onChangeValue = (p1: string, p2: string) => {
    // @ts-ignore
    onChange(p1 + p2);
  };

  return (
    <>
      <Input.Group compact>
        <Input
          style={{ width: '50%' }}
          placeholder="默认为空"
          value={firstPath}
          onChange={(val) => {
            const path = val.target.value;
            setFirstPath(path);
            onChangeValue(path, secondPath);
          }}
        />
        <Input
          style={{ width: '50%' }}
          value={secondPath}
          onChange={(val) => {
            const path = val.target.value;
            setSecondPath(path);
            onChangeValue(firstPath, path);
          }}
        />
      </Input.Group>
    </>
  );
};

export default InputPath;
