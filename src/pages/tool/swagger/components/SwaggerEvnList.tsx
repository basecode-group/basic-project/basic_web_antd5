import React, {
  useState,
  useEffect,
  useRef,
  ReactText,
  forwardRef,
  useImperativeHandle,
} from 'react';
import ProTable, { ActionType } from '@ant-design/pro-table';
import { Input, Button, message, Popconfirm } from 'antd';
import { getSwaggerEvnList } from '@/services/tool/swagger';
import { DeleteOutlined } from '@ant-design/icons';
import { dictDelete } from '@/services/system/dict';
import { saveMenu4Role } from '@/services/system/menu';
import SwaggerEnvEdit from './SwaggerEnvEdit';

const { Search } = Input;

// 定义传参
interface SwaggerEvnListParams {
  setEvnRows: (rows: ReactText[]) => void;
}

/**
 * swagger 环境地址管理
 *
 * @author zhangby
 * @date 16/10/20 5:06 pm
 */
const SwaggerEvnList: React.FC<SwaggerEvnListParams> = (props, reff) => {
  const { setEvnRows } = props;
  const [query, setQuery] = useState<string>('');
  const [rows, setRows] = useState<ReactText[]>([]);
  const [loading, setLoading] = useState(false);

  const ref = useRef<ActionType>();

  // 删除
  const onDelete = async (id: string) => {
    try {
      setLoading(true);
      await dictDelete(id);
      setLoading(false);
      message.success('删除记录成功！');
      // 刷新
      // @ts-ignore
      ref.current?.reloadAndRest();
    } catch (e) {
      setLoading(false);
    }
  };

  // 搜索
  const onSearch = (val?: string) => {
    // @ts-ignore
    setQuery(val);
    ref.current?.reload();
  };

  useImperativeHandle(reff, () => ({
    // changeVal 就是暴露给父组件的方法
    refresh: async () => {
      // 提交
      onSearch();
    },
    onBatchDelete: () => {
      if (rows.length === 0) {
        message.error('请选择要操作的记录');
      }
      onDelete(rows.join(','));
    },
  }));

  return (
    <>
      {/* 列表 */}
      <ProTable<Tool.SysCron>
        actionRef={ref}
        search={false}
        columns={[
          {
            title: '环境名称',
            dataIndex: 'label',
            align: 'center',
            width: 150,
          },
          {
            title: '环境地址',
            dataIndex: 'value',
            align: 'center',
            width: 150,
          },
          {
            title: '排序',
            dataIndex: 'sort',
            align: 'center',
            width: 150,
          },
          {
            title: '备注信息',
            dataIndex: 'description',
            align: 'center',
            width: 150,
          },
          {
            title: '操作',
            dataIndex: 'id',
            align: 'center',
            width: 150,
            render: (_, record) => (
              <>
                <SwaggerEnvEdit refresh={onSearch} evnId={record.id} />
                <Popconfirm
                  placement="topRight"
                  title="确定要删除选中的记录吗?"
                  onConfirm={() => onDelete(record.id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button type="primary" shape="circle" danger style={{ marginLeft: 10 }}>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </>
            ),
          },
        ]}
        rowSelection={{
          columnWidth: 20,
          onChange: (selectedRowKeys, _) => {
            setRows(selectedRowKeys);
            if (setEvnRows) {
              // @ts-ignore
              setEvnRows(selectedRowKeys);
            }
          },
        }}
        request={(params, sorter, filter) =>
          getSwaggerEvnList({ ...{ ...params, keyword: query }, sorter, filter })
        }
        pagination={{
          pageSize: 10,
        }}
        rowKey="id"
        headerTitle="Swagger 环境设置"
        toolBarRender={() => [
          <Search placeholder="搜索" onSearch={onSearch} style={{ minWidth: 400 }} enterButton />,
        ]}
      />
    </>
  );
};

// @ts-ignore
export default forwardRef(SwaggerEvnList);
