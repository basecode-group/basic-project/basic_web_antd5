import React, { useState, useEffect } from 'react';
import { Button, Modal, Spin, Form, Input, InputNumber, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getSwaggerEvnMaxSort, getSwaggerEvnParent } from '@/services/tool/swagger';
import { dictAdd } from '@/services/system/dict';

// 定义传参
interface SwaggerEnvAddParams {
  refresh?: () => void;
}

/**
 * swagger 环境编辑
 *
 * @author zhangby
 * @date 16/10/20 5:44 pm
 */
const SwaggerEnvAdd: React.FC<SwaggerEnvAddParams> = (props) => {
  const { children, refresh } = props;
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [parentId, setParentId] = useState<string>('');

  const [form] = Form.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    // 查询最大排序
    try {
      setLoading(true);
      const maxSort = await getSwaggerEvnMaxSort();
      if (maxSort.code === '000') {
        form.setFieldsValue({
          sort: maxSort.result?.maxSort,
        });
      }
      // 查询父类
      const parentResult = await getSwaggerEvnParent();
      if (parentResult.code === '000') {
        // @ts-ignore
        setParentId(parentResult.result?.id);
      }
      setLoading(false);
      //
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    // 提交form
    form.validateFields().then(async (values) => {
      try {
        const addResult = await dictAdd({ ...values, parentId });
        if (addResult.code === '000') {
          message.success('新建环境成功');
          setLoading(false);
          setVisible(false);
          if (refresh) {
            refresh();
          }
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <span style={{ marginLeft: 10, cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary">
            <PlusOutlined />
            新建环境
          </Button>
        )}
      </span>

      {/* 弹出窗口 */}
      <Modal
        title="新建环境"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={600}
      >
        <Spin spinning={loading}>
          <Form {...layout} form={form}>
            <Form.Item
              name="label"
              label="环境名称"
              rules={[{ required: true, message: '环境名称不能为空' }]}
            >
              <Input placeholder="环境名称" />
            </Form.Item>
            <Form.Item
              name="value"
              label="地址"
              rules={[{ required: true, message: '地址不能为空' }]}
            >
              <Input placeholder="环境名称" />
            </Form.Item>
            <Form.Item name="sort" label="排序" initialValue={10}>
              <InputNumber min={0} step={1} precision={0} placeholder="排序" />
            </Form.Item>
            <Form.Item name="description" label="描述">
              <Input.TextArea placeholder="字典描述" autoSize={{ minRows: 3, maxRows: 5 }} />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default SwaggerEnvAdd;
