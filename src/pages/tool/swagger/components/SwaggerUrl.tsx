import React, { useState, useEffect } from 'react';
import { getSwaggerUrl } from '@/services/tool/swagger';
import { Select } from 'antd';

const { Option } = Select;

// 定义传参
interface SwaggerUrlParams {
  onRender: (url: string) => void;
}

/**
 * swagger 地址管理
 *
 * @author zhangby
 * @date 16/10/20 3:22 pm
 */
const SwaggerUrl: React.FC<SwaggerUrlParams> = (props) => {
  const { onRender } = props;
  const [urlData, setUrlData] = useState<any[]>([]);
  const [keyUrl, setKeyUrl] = useState<string>('');

  // 初始化
  useEffect(() => {
    getSwaggerUrl().then((res) => {
      if (res) {
        setUrlData(res);
        let swaggerUrl = localStorage.getItem('SwaggerUrl');
        if (!swaggerUrl) {
          swaggerUrl = res[0]?.url;
        }
        // 设置默认选项
        // @ts-ignore
        setKeyUrl(swaggerUrl);
        // 回调
        if (onRender) {
          // @ts-ignore
          onRender(swaggerUrl);
        }
      }
    });
  }, []);

  // 刷新地址
  const onChangeUrl = (url: string) => {
    setKeyUrl(url);
    // 回调
    if (onRender) {
      onRender(url);
    }
    localStorage.setItem('SwaggerUrl', url);
  };

  return (
    <>
      <span>
        <span style={{ marginRight: 10 }}>Select a spec</span>
        <Select
          style={{ width: 260 }}
          value={keyUrl}
          onChange={(val) => {
            onChangeUrl(val);
          }}
        >
          {urlData &&
            urlData.map((item, key) => (
              <Option value={item.url} key={key}>
                {item.name}
              </Option>
            ))}
        </Select>
      </span>
    </>
  );
};

export default SwaggerUrl;
