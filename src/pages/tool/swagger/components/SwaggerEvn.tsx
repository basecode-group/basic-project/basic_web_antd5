import React, { useState, useEffect } from 'react';
import { Select } from 'antd';
import { getDictList4Type } from '@/services/system/dict';

const { Option } = Select;

// 定义传参
interface SwaggerEvnParams {
  onRender: (url: string) => void;
}

/**
 * swagger 环境管理
 *
 * @author zhangby
 * @date 16/10/20 4:02 pm
 */
const SwaggerEvn: React.FC<SwaggerEvnParams> = (props) => {
  const { onRender } = props;
  const [envData, setEnvData] = useState<API.DictMap[]>([]);
  const [envKey, setEnvKey] = useState<string>('');

  // 初始化
  useEffect(() => {
    getDictList4Type('api_url_config').then((res) => {
      if (res) {
        const data = res.result;
        // @ts-ignore
        setEnvData(data);
        let swaggerEvn = localStorage.getItem('SwaggerEvn');
        if (!swaggerEvn) {
          swaggerEvn = `${data[0]?.value}~${data[0]?.label}`;
        }
        // 设值
        // @ts-ignore
        setEnvKey(swaggerEvn);
        // @ts-ignore
        onRender(swaggerEvn.split('~')[0]);
      }
    });
  }, []);

  // 刷新结果
  const onChangeEvn = (value: string) => {
    setEnvKey(value);
    onRender(value.split('~')[0]);
    // 设置缓存
    // localStorage('SwaggerEvn', value);
  };

  return (
    <>
      <span style={{ marginRight: 10 }}>Environment</span>
      <Select
        value={envKey}
        onChange={(val) => {
          onChangeEvn(val);
          localStorage.setItem('SwaggerEvn', val);
        }}
        bordered={false}
      >
        {envData &&
          envData.map((item, key) => (
            <Option value={item.value + '~' + item.label} key={key}>
              {item.label}
            </Option>
          ))}
      </Select>
    </>
  );
};

export default SwaggerEvn;
