import React, { useState, useEffect, useRef, ReactText } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Typography, Spin, Card, Empty, Button, Popconfirm } from 'antd';
import SwaggerUrl from './components/SwaggerUrl';
import SwaggerUI from 'swagger-ui-react';
import 'swagger-ui-react/swagger-ui.css';
import SwaggerEvn from '@/pages/tool/swagger/components/SwaggerEvn';
import './swagger.less';
import {
  SafetyCertificateOutlined,
  LogoutOutlined,
  ReloadOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import SwaggerEvnList from './components/SwaggerEvnList';
import SwaggerEnvAdd from './components/SwaggerEnvAdd';

const { Text } = Typography;

/**
 * Swagger 文档
 *
 * @author zhangby
 * @date 16/10/20 3:10 pm
 */
export default (): React.ReactNode => {
  const [loading, setLoading] = useState(false);
  const [url, setUrl] = useState<string>('');
  const [key, setKey] = useState<boolean>(false);
  const [host, setHost] = useState<string>('');
  const [token, setToken] = useState<string>('');
  const [keyTab, setKeyTab] = useState<string>('1');
  const [rows, setRows] = useState<ReactText[]>([]);

  // 初始化
  useEffect(() => {
    // @ts-ignore
    setToken(localStorage.getItem('token'));
  }, []);

  // 重新授权
  const fastAuth = () => {
    // @ts-ignore
    setToken(localStorage.getItem('token'));
    // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
    refreshApi();
  };

  // 清空授权
  const clearAuth = () => {
    setToken('');
    // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
    refreshApi();
  };

  // 刷新文档
  const refreshApi = () => {
    setKey(!key);
  };

  const childRef = useRef();

  // 刷新环境列表
  const refreshEvnList = () => {
    // @ts-ignore
    childRef.current?.refresh();
  };

  // 批量删除
  const onBatchDelEvn = () => {
    // @ts-ignore
    childRef.current?.onBatchDelete();
    setRows([]);
  };

  return (
    <>
      <PageContainer
        subTitle="接口文档，是基于 Swagger 来封装的。"
        extra={[
          <span key="select">
            {keyTab === '1' ? <SwaggerUrl key="url" onRender={setUrl} /> : null}
          </span>,
          <span key="del">
            {keyTab === '2' && rows.length > 0 ? (
              <Popconfirm
                title="确定要删除选中的记录吗?"
                onConfirm={onBatchDelEvn}
                okText="Yes"
                cancelText="No"
              >
                <Button type="primary" danger>
                  <DeleteOutlined />
                  批量删除
                </Button>
              </Popconfirm>
            ) : null}
          </span>,
          <span key="add">
            {keyTab === '2' ? <SwaggerEnvAdd refresh={refreshEvnList} /> : null}
          </span>,
        ]}
        tabList={[
          {
            tab: '接口文档',
            key: '1',
          },
          {
            tab: '环境配置',
            key: '2',
          },
        ]}
        tabActiveKey={keyTab}
        onTabChange={(keyVal) => setKeyTab(keyVal)}
      >
        {keyTab === '1' ? (
          <Spin spinning={loading}>
            <Card style={{ background: '#fafafa', maxWidth: 1100, margin: '0px auto' }}>
              {url ? (
                <div style={{ position: 'relative' }}>
                  <div style={{ textAlign: 'left', marginBottom: 20 }}>
                    <SwaggerEvn onRender={setHost} />
                  </div>

                  <div style={{ position: 'absolute', top: 0, right: 0 }}>
                    <Button type="link" style={{ color: '#414141' }} onClick={fastAuth}>
                      <SafetyCertificateOutlined /> 快速授权
                    </Button>
                    <Button type="link" style={{ color: '#414141' }} onClick={clearAuth}>
                      <LogoutOutlined /> 清空授权
                    </Button>
                    <Button type="link" style={{ color: '#414141' }} onClick={refreshApi}>
                      <ReloadOutlined /> 刷新
                    </Button>
                  </div>
                  {host ? (
                    <SwaggerUI
                      key={key + host + url}
                      url={host + url}
                      docExpansion={'none'}
                      onComplete={(swaggerUi: any) => {
                        swaggerUi.preauthorizeApiKey('Authorization', token);
                      }}
                    />
                  ) : null}
                </div>
              ) : (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
              )}
            </Card>
          </Spin>
        ) : (
          <SwaggerEvnList ref={childRef} setEvnRows={setRows} />
        )}
      </PageContainer>
    </>
  );
};
