import React, { useState, useEffect, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { getMenuById } from '@/services/system/menu';
import { Empty, Card } from 'antd';

/**
 * 工具管理
 * @returns {JSX.Element}
 */
export default (props): React.ReactNode => {
  const ref = useRef();
  const [frameHeight, setFrameHeight] = useState<string>('100%');
  const [url, setUrl] = useState<string>('');

  // 查询菜单目标路由
  useEffect(() => {
    const menuId = props.location.query.menuId;
    if (menuId) {
      getMenuById(menuId).then((res) => {
        if (res) {
          setUrl(res.result.target);
        }
      });
    }
  }, []);

  // @ts-ignore
  return (
    <PageContainer style={{ height: '100%' }}>
      {url ? (
        <iframe
          ref={ref}
          src={url}
          onLoad={() => {
            setFrameHeight(ref.current?.offsetParent?.clientHeight + 'px');
          }}
          width="100%"
          height={frameHeight}
          scrolling="yes"
          frameBorder="0"
        />
      ) : (
        <Card>
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        </Card>
      )}
    </PageContainer>
  );
};
