import React, { useState, useEffect } from 'react';
import { Badge } from 'antd';

// 定义传参
interface StatusTextParams {
  value?: string;
}

/**
 * 状态类型
 *
 * @author zhangby
 * @date 15/10/20 4:17 pm
 */
const StatusText: React.FC<StatusTextParams> = (props) => {
  const { value } = props;
  const [status, setStatus] = useState<string>('0');

  // 初始化
  useEffect(() => {
    if (value) {
      setStatus(value);
    }
  }, [value]);

  return (
    <>
      {status === '0' ? (
        <span style={{ color: '#108ee9' }}>
          <Badge status="processing" />
          启用
        </span>
      ) : (
        <span style={{ color: '#f50' }}>
          <Badge status="error" />
          停用
        </span>
      )}
    </>
  );
};

export default StatusText;
