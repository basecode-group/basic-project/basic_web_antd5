import React from 'react';
import './css/SysText.less';

// 定义传参
interface SysTextParams {
  style?: any;
}

/**
 * 系统文本
 *
 * @author zhangby
 * @date 14/10/20 5:52 pm
 */
const SysText: React.FC<SysTextParams> = (props) => {
  const { children, style } = props;

  // @ts-ignore
  return (
    <span className="ant-tag-text" style={style}>
      {children}
    </span>
  );
};

export default SysText;
