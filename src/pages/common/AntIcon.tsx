import React from 'react';
import { createFromIconfontCN } from '@ant-design/icons';

// 定义传参
interface AntIconParams {
  onClick?: () => void;
  type: string;
  style?: any;
}

/**
 * Ant 图标
 *
 * @author zhangby
 * @date 24/9/20 12:00 pm
 */
const AntIcon: React.FC<AntIconParams> = (props) => {
  const { type, style, onClick } = props;

  const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_1533614_1iplsjhq8eg.js',
  });

  return (
    <>
      <IconFont type={type} style={{ fontSize: '1em', ...style }} onClick={onClick} />
    </>
  );
};

export default AntIcon;
