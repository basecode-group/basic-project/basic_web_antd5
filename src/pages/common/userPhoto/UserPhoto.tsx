import React, { useState, useEffect } from 'react';
import { Avatar, Modal, Radio } from 'antd';
import UserPhotoSystemParams from './UserPhotoSystem';
import UserPhotoView from './UserPhotoView';
import UserPhotoColor from '@/pages/common/userPhoto/UserPhotoColor';
import UserPhotoUpload from '@/pages/common/userPhoto/UserPhotoUpload';

// 定义传参
interface UserPhotoParams {
  isEdit?: boolean;
  size?: number;
  photo?: string;
  value?: string;
  onChange?: (value: string) => void;
}

// 用户头像数据
interface UserPhotoData {
  select?: boolean;
  key?: number;
  fontColor?: string;
  fontValue?: string;
  background?: string;
  upload?: boolean;
  photoUrl?: string;
}

/**
 * 用户头像
 *
 * @author zhangby
 * @date 24/9/20 6:36 pm
 */
const UserPhoto: React.FC<UserPhotoParams> = (props) => {
  const { isEdit, size, photo, value, onChange } = props;
  const [visible, setVisible] = useState(false);
  const [tabsType, setTabsType] = useState<string>('1');
  const [photoData, setPhotoData] = useState<UserPhotoData>({
    select: true,
    key: 0,
    fontColor: '#ffffff',
    background: '#cccccc',
    fontValue: 'USER',
    upload: false,
  });

  // 初始化
  useEffect(() => {
    try {
      if (value) {
        const photoJson = JSON.parse(value);
        // 设置选项
        if (photoJson.upload) {
          setTabsType('3');
        } else if (!photoJson.select) {
          setTabsType('2');
        } else {
          setTabsType('1');
        }
        setPhotoData(photoJson);
      }
    } catch (e) {
      console.log(e);
      setPhotoData({
        select: true,
        key: 0,
        fontColor: '#ffffff',
        background: '#cccccc',
        fontValue: 'USER',
        upload: false,
      });
    }
  }, [value]);

  // 初始化
  const onPreInit = () => {
    // 是否可编辑
    if (isEdit) {
      setVisible(true);
      // 设置头像
      // if (photo) {
      //   try {
      //     // 格式化
      //     setPhotoData(JSON.parse(photo));
      //   } catch (e) {
      //     // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
      //     setDefaultPhoto({
      //       select: true,
      //       key: 0,
      //     });
      //   }
      // } else {
      //   // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
      //   setDefaultPhoto({
      //     select: true,
      //     key: 0,
      //   });
      // }
    }
  };

  // 初始化头像
  const setDefaultPhoto = (item: UserPhotoData) => {
    setPhotoData({ ...photoData, ...item });
  };

  // 设置tab
  const setTabs = (val: string) => {
    if (val === '1') {
      setDefaultPhoto({ ...photoData, select: true, upload: false });
    } else if (val === '2') {
      setDefaultPhoto({ ...photoData, select: false, upload: false });
    } else if (val === '3') {
      setDefaultPhoto({ ...photoData, select: false, upload: true });
    }
    setTabsType(val);
  };

  // 提交
  const onSubmit = () => {
    // 更新
    if (onChange) {
      onChange(JSON.stringify(photoData));
    }
    setVisible(false);
  };

  // 返回
  return (
    <>
      <span onClick={onPreInit} style={{ cursor: isEdit ? 'pointer' : '' }}>
        <UserPhotoView size={size} photo={photoData} />
      </span>

      <Modal
        title="选择头像"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={700}
      >
        <div style={{ textAlign: 'center', marginBottom: 20 }}>
          <UserPhotoView size={70} photo={photoData} />
        </div>
        <div style={{ textAlign: 'center' }}>
          <Radio.Group value={tabsType} onChange={(val) => setTabs(val.target.value)}>
            <Radio.Button value="1">系统头像</Radio.Button>
            <Radio.Button value="2">自定义头像</Radio.Button>
            <Radio.Button value="3">上传头像</Radio.Button>
          </Radio.Group>
        </div>
        {/* 选择头像 */}
        <div style={{ marginTop: 20 }}>
          {/* eslint-disable-next-line no-nested-ternary */}
          {tabsType === '1' ? (
            <UserPhotoSystemParams onRender={setDefaultPhoto} />
          ) : tabsType === '2' ? (
            <UserPhotoColor onRender={setDefaultPhoto} />
          ) : (
            <UserPhotoUpload photo={photoData} onRender={setDefaultPhoto} />
          )}
        </div>
      </Modal>
    </>
  );
};

// 设置默认值
UserPhoto.defaultProps = {
  size: 40,
  isEdit: false,
};

export default UserPhoto;
