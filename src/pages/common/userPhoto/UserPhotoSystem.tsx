import React, { useState, useEffect } from 'react';
import icons1 from '@/assets/avatars/1.jpg';
import icons2 from '@/assets/avatars/2.jpg';
import icons3 from '@/assets/avatars/3.jpg';
import icons4 from '@/assets/avatars/4.jpg';
import icons5 from '@/assets/avatars/5.jpg';
import icons6 from '@/assets/avatars/6.jpg';
import icons7 from '@/assets/avatars/7.jpg';
import icons8 from '@/assets/avatars/8.jpg';
import icons9 from '@/assets/avatars/9.png';
import icons10 from '@/assets/avatars/10.jpeg';
import icons11 from '@/assets/avatars/11.jpeg';
import icons12 from '@/assets/avatars/icons12.png';
import { Row, Col, Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';

// 定义传参
interface UserPhotoSystemParams {
  size?: number;
  onRender?: (item: any) => {};
}

/**
 * 系统头像管理
 *
 * @author zhangby
 * @date 25/9/20 10:27 am
 */
const UserPhotoSystem: React.FC<UserPhotoSystemParams> = (props) => {
  const { size, onRender } = props;

  // 创建
  const onClick = (key: number) => {
    const result = { select: true, upload: false, key };
    // 回调
    if (onRender) {
      onRender(result);
    }
  };

  // 默认头像
  const defaultIcons = [
    <Avatar size={size} key={-2} icon={<UserOutlined />} />,
    <Avatar size={size} style={{ backgroundColor: '#00a2ae' }}>
      Admin
    </Avatar>,
    <Avatar size={size} style={{ backgroundColor: '#FF6900' }}>
      USER
    </Avatar>,
    <Avatar
      size={size}
      style={{
        color: '#f56a00',
        backgroundColor: '#fde3cf',
      }}
    >
      U
    </Avatar>,
    <Avatar size={size} src={icons12} />,
    <Avatar size={size} src={icons1} />,
    <Avatar size={size} src={icons2} />,
    <Avatar size={size} src={icons3} />,
    <Avatar size={size} src={icons4} />,
    <Avatar size={size} src={icons5} />,
    <Avatar size={size} src={icons6} />,
    <Avatar size={size} src={icons7} />,
    <Avatar size={size} src={icons8} />,
    <Avatar size={size} src={icons9} />,
    <Avatar size={size} src={icons10} />,
    <Avatar size={size} src={icons11} />,
  ];

  return (
    <>
      <Row gutter={10} style={{ marginTop: 20, textAlign: 'center' }}>
        {defaultIcons.map((item, key) => (
          <Col
            md={3}
            xs={6}
            key={key}
            style={{ margin: '8px 0 8px 0', cursor: 'pointer' }}
            onClick={() => onClick(key)}
          >
            {item}
          </Col>
        ))}
      </Row>
    </>
  );
};

// 设置默认值
UserPhotoSystem.defaultProps = {
  size: 35,
};

export default UserPhotoSystem;
