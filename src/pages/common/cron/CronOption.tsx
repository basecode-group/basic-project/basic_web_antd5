import React, { useState, useEffect } from 'react';
import { Row, Col, Popconfirm, Button, message } from 'antd';
import {
  QuestionCircleOutlined,
  PlusOutlined,
  PauseOutlined,
  CaretRightOutlined,
  RedoOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import { jobCreate, jobPause, jobResume, jobRun, jobDelete } from '@/services/tool/cron';

// 定义传参
interface CronOptionParams {
  value?: string;
  onRender?: () => void;
}

/**
 * 定时任务操作
 *
 * @author zhangby
 * @date 15/10/20 5:09 pm
 */
const CronOption: React.FC<CronOptionParams> = (props) => {
  const { value, onRender } = props;
  const [loading, setLoading] = useState(false);

  // 创建 Job
  const onJobCreate = async () => {
    try {
      if (!value) {
      }
      setLoading(true);
      const jobResult = await jobCreate(value);
      if (jobResult.code === '000') {
        message.success('创建定时任务成功');
        if (onRender) {
          onRender();
        }
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 暂停 Job
  const onJobPause = async () => {
    try {
      setLoading(true);
      const jobResult = await jobPause(value);
      if (jobResult.code === '000') {
        message.success('暂停定时任务成功');
        if (onRender) {
          onRender();
        }
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 恢复 Job
  const onJobResume = async () => {
    try {
      setLoading(true);
      const jobResult = await jobResume(value);
      if (jobResult.code === '000') {
        message.success('恢复定时任务成功');
        if (onRender) {
          onRender();
        }
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 运行一次 Job
  const onJobRun = async () => {
    try {
      setLoading(true);
      const jobResult = await jobRun(value);
      if (jobResult.code === '000') {
        message.success('运行定时任务成功');
        if (onRender) {
          onRender();
        }
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  // 删除 Job
  const onJobDelete = async () => {
    try {
      setLoading(true);
      const jobResult = await jobDelete(value);
      if (jobResult.code === '000') {
        message.success('删除定时任务成功');
        if (onRender) {
          onRender();
        }
      }
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  return (
    <>
      <h3>操作</h3>
      <Row style={{ marginTop: 20 }}>
        <Col span={4}>
          <Popconfirm
            onConfirm={onJobCreate}
            title="确定要创建定时任务吗？"
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <Button type="primary" shape="circle" icon={<PlusOutlined />} />
            <span style={{ marginLeft: 10 }}>创建</span>
          </Popconfirm>
        </Col>
        <Col span={4}>
          <Popconfirm
            onConfirm={onJobPause}
            title="确定要暂停定时任务吗？"
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <Button type="primary" shape="circle" icon={<PauseOutlined />} />
            <span style={{ marginLeft: 10 }}>暂停</span>
          </Popconfirm>
        </Col>
        <Col span={4}>
          <Popconfirm
            onConfirm={onJobResume}
            title="确定要恢复定时任务吗？"
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <Button type="primary" shape="circle" icon={<CaretRightOutlined />} />
            <span style={{ marginLeft: 10 }}>恢复</span>
          </Popconfirm>
        </Col>
        <Col span={4}>
          <Popconfirm
            onConfirm={onJobRun}
            title="确定要执行定时任务吗？"
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <Button type="primary" shape="circle" icon={<RedoOutlined />} />
            <span style={{ marginLeft: 10 }}>单次执行</span>
          </Popconfirm>
        </Col>
        <Col span={4} offset={1}>
          <Popconfirm
            onConfirm={onJobDelete}
            title="确定要删除定时任务吗？"
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <Button type="primary" danger shape="circle" icon={<DeleteOutlined />} />
            <span style={{ marginLeft: 10 }}>删除</span>
          </Popconfirm>
        </Col>
      </Row>
    </>
  );
};

export default CronOption;
