import React, { useState, useEffect } from 'react';
import { Button, Drawer, Modal, message } from 'antd';
import Cron from 'cron-editor-react';
import CronNextDate from '@/pages/common/cron/CronNextDate';
import { jobUpdate } from '@/services/tool/cron';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const { confirm } = Modal;

// 定义传参
interface CronEditParams {
  isEdit?: boolean;
  jobName?: string;
  value: string;
  onRender?: () => void;
}

/**
 * 编辑定时任务 (此组件只负责，更新当前定时任务)
 *
 * @author zhangby
 * @date 16/10/20 10:46 am
 */
const CronEdit: React.FC<CronEditParams> = (props) => {
  const { children, isEdit, value, onRender, jobName } = props;
  const [visible, setVisible] = useState(false);
  const [cron, setCron] = useState<string>('');
  const [cronTemp, setCronTemp] = useState<string>('');

  // 加载
  useEffect(() => {
    if (value) {
      setCron(value);
      setCronTemp(value);
    }
  }, [value]);

  // 初始化
  const onPreInit = () => {
    setVisible(true);
  };

  // 提交
  const onSubmit = () => {
    confirm({
      title: '确定要更新定时任务吗?',
      icon: <ExclamationCircleOutlined />,
      content: '定时任务只更新内存，不会同步更新数据库。',
      onOk() {
        try {
          // 更新cron
          jobUpdate({ jobName, cron: cronTemp }).then((res) => {
            if (res.code === '000') {
              setCron(cronTemp);
              message.success('更新定时任务成功');
              setVisible(false);
              // 刷新回调
              if (onRender) {
                onRender();
              }
            }
          });
        } catch (e) {
          console.log(e);
        }
      },
    });
  };

  // 变更表达式
  const onChangeCron = (cronVal: string) => {
    setCronTemp(cronVal);
  };

  return (
    <>
      <span style={{ marginLeft: 10, marginRight: 10, cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="link" style={{ margin: 0, padding: 0 }}>
            {cron}
          </Button>
        )}
      </span>
      {/* 弹出窗口 */}
      <Drawer
        title="编辑 Cron 表达式"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            {isEdit || !jobName ? (
              <Button onClick={onSubmit} type="primary">
                确定
              </Button>
            ) : null}
          </div>
        }
      >
        <Cron showCrontab value={cronTemp} onChange={onChangeCron} />
        <CronNextDate value={cronTemp} />
      </Drawer>
    </>
  );
};

// 初始值
CronEdit.defaultProps = {
  isEdit: true,
};

export default CronEdit;
