import React, { useState, useEffect } from 'react';
import { Drawer, Button, Spin, Row, Col } from 'antd';
import AntIcon from '@/pages/common/AntIcon';
import { getCronNextDate, getCronById } from '@/services/tool/cron';
import ReactJson from 'react-json-view';
import CronOption from '@/pages/common/cron/CronOption';
import CronEdit from '@/pages/common/cron/CronEdit';

// 定义传参
interface CronConfigParams {
  cronId: string;
}

/**
 * 定时任务 管理
 *
 * @author zhangby
 * @date 15/10/20 4:23 pm
 */
const CronConfig: React.FC<CronConfigParams> = (props) => {
  const { children, cronId } = props;
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [nextDate, setNextDate] = useState<string[]>([]);
  const [cron, setCron] = useState<Tool.SysCron>();
  const [refresh, setRefresh] = useState<boolean>(false);

  // 初始化
  const onPreInit = () => {
    setVisible(true);
    // 查询记录
    // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
    getCronDetail(cronId);
  };

  // 查询
  const getCronDetail = (id: string) => {
    try {
      setLoading(true);
      getCronById(id).then((res) => {
        if (res) {
          setCron(res.result);
          // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
          initCronNextDate(res.result.tails.currentCron);
          setRefresh(!refresh);
        }
        setLoading(false);
      });
    } catch (e) {
      setLoading(false);
    }
  };

  // 查询定时任务
  const initCronNextDate = (currentCron: string) => {
    try {
      if (currentCron) {
        setLoading(true);
        getCronNextDate(currentCron).then((nextDateResult) => {
          if (nextDateResult.code === '000') {
            // @ts-ignore
            setNextDate(nextDateResult.result);
          }
        });
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {};

  // 返回
  // @ts-ignore
  return (
    <>
      <span onClick={onPreInit}>{children}</span>
      {/* 弹出窗口 */}
      <Drawer
        title="定时任务详情"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            {/*<Button onClick={onSubmit} type="primary">*/}
            {/*  确定*/}
            {/*</Button>*/}
          </div>
        }
      >
        <Spin spinning={loading}>
          <h3>定时任务</h3>
          <Row style={{ marginTop: 30, marginBottom: 20 }}>
            <Col span={24}>
              <AntIcon
                type="icon-biaoji"
                style={{ fontSize: '1.4em', float: 'left', marginRight: 10 }}
              />
              <DescriptionItem
                title="定时任务名称"
                content={
                  <span style={{ color: '#eb2f96' }}>
                    {cron?.name} （ {cron?.mark} ）
                  </span>
                }
              />
            </Col>
            <Col span={24}>
              <AntIcon
                type="icon-jiandangzhuangtai-copy-copy"
                style={{ fontSize: '1.6em', float: 'left', marginRight: 10, marginLeft: -2 }}
              />
              <DescriptionItem
                title="定时任务状态"
                style={{ marginTop: -15 }}
                content={
                  <span style={{ color: '#eb2f96' }}>
                    {cron?.tails.cronStatus.label} ( {cron?.tails.cronStatus.value} )
                    <span style={{ color: '#1890FF' }}>
                      【
                      <CronEdit
                        key={refresh}
                        value={cron?.tails.currentCron}
                        jobName={cron?.mark}
                        onRender={() => getCronDetail(cron?.id)}
                      />
                      】
                    </span>
                  </span>
                }
              />
            </Col>
            <Col span={24}>
              <AntIcon
                type="icon-zhengzebiaodashi"
                style={{
                  fontSize: '1.1em',
                  float: 'left',
                  marginRight: 15,
                  marginLeft: 1,
                  marginTop: 3,
                }}
              />
              <DescriptionItem
                title="Cron 表达式 ( 数据库 )"
                content={
                  <span style={{ color: '#1890FF' }}>
                    {/*<EditCron key={visible} cron={value?.cron} jobName={value?.mark} />*/}
                    <span style={{ marginLeft: 20, color: '#c41d7f' }}>{cron?.cron}</span>
                  </span>
                }
              />
            </Col>
            <Col span={24}>
              <AntIcon
                type="icon-zhengzebiaodashi"
                style={{
                  fontSize: '1.1em',
                  float: 'left',
                  marginRight: 15,
                  marginLeft: 1,
                  marginTop: 3,
                }}
              />
              <DescriptionItem
                title="Cron 表达式 （ 初始CRON ）"
                content={
                  <span style={{ color: '#1890FF' }}>
                    <span style={{ marginLeft: 20, color: '#c41d7f' }}>{cron?.tails.oldCron}</span>
                  </span>
                }
              />
            </Col>
            <Col span={24}>
              {cron?.tails.cronStatus?.value === 'NORMAL' ? (
                <ReactJson displayDataTypes={false} name="近5次执行时间" src={nextDate} />
              ) : null}
            </Col>
          </Row>
          {/* 操作 */}
          <CronOption value={cron?.mark} onRender={() => getCronDetail(cron?.id)} />
        </Spin>
      </Drawer>
    </>
  );
};

const DescriptionItem = ({ title, content, style }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: '22px',
      marginBottom: 0,
      color: 'rgba(0,0,0,0.65)',
      ...style,
    }}
  >
    <p
      style={{
        marginRight: 8,
        marginBottom: 13,
        display: 'inline-block',
        color: 'rgba(0,0,0,0.85)',
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);

export default CronConfig;
