import React, { useState, useEffect } from 'react';
import { Collapse, List } from 'antd';
import { getCronNextDate } from '@/services/tool/cron';

const { Panel } = Collapse;

// 定义传参
interface CronNextDateParams {
  value?: string;
}

/**
 * 定时任务下级
 *
 * @author zhangby
 * @date 15/10/20 3:06 pm
 */
const CronNextDate: React.FC<CronNextDateParams> = (props) => {
  const { value } = props;
  const [nextDate, setNextDate] = useState<string[]>([]);

  // 初始化
  useEffect(() => {
    if (value) {
      // eslint-disable-next-line no-use-before-define,@typescript-eslint/no-use-before-define
      getCronDate(value);
    }
  }, [value]);

  // 调用接口
  const getCronDate = (cron: string) => {
    getCronNextDate(cron).then((res) => {
      if (res.code === '000') {
        // @ts-ignore
        setNextDate(res.result);
      }
    });
  };

  return (
    <>
      <Collapse style={{ marginTop: 10 }} defaultActiveKey={['1']}>
        <Panel header="近5次执行时间" key="1">
          <List
            bordered
            dataSource={nextDate}
            renderItem={(item, key) => <List.Item>{`第${key + 1}执行时间：${item}`}</List.Item>}
          />
        </Panel>
      </Collapse>
    </>
  );
};

export default CronNextDate;
