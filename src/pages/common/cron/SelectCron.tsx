import React, { useEffect, useState } from 'react';
import { Button, Drawer, Input } from 'antd';
// @ts-ignore
import Cron from 'cron-editor-react';
import CronNextDate from './CronNextDate';

// 定义传参
interface SelectCronParams {
  value?: string;
  onChange?: (value: string) => void;
}

/**
 * 选择定时任务
 *
 * @author zhangby
 * @date 15/10/20 2:39 pm
 */
const SelectCron: React.FC<SelectCronParams> = (props) => {
  const { value, onChange } = props;
  const [visible, setVisible] = useState(false);
  const [cron, setCron] = useState<string>('* * * * * ?');
  const [cronTemp, setCronTemp] = useState<string>('* * * * * ?');

  // 加载
  useEffect(() => {
    if (value !== cron) {
      // 初始化
      // @ts-ignore
      setCron(value);
      // @ts-ignore
      setCronTemp(value);
    }
  }, [value]);

  // 初始化
  const onPreInit = () => {
    setVisible(true);
    if (value !== cronTemp) {
      // 初始化
      // @ts-ignore
      setCron(value);
      // @ts-ignore
      setCronTemp(value);
    }
  };

  // 变更表达式
  const onChangeCron = (cronVal: string) => {
    setCronTemp(cronVal);
  };

  // 操作
  const onSubmit = () => {
    if (onChange) {
      onChange(cronTemp);
      setCron(cronTemp);
    }
    setVisible(false);
  };

  return (
    <>
      <Input.Group compact>
        <Input style={{ width: '75%' }} disabled placeholder="cron 表达式" value={cron} />
        <Button style={{ width: '25%' }} type="primary" onClick={onPreInit}>
          CRON
        </Button>
      </Input.Group>
      {/* 弹出窗口 */}
      <Drawer
        title="选择Cron表达式"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Cron showCrontab value={cronTemp} onChange={onChangeCron} />
        <CronNextDate value={cronTemp} />
      </Drawer>
    </>
  );
};

export default SelectCron;
