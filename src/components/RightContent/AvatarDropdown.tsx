import React, { useCallback, useRef } from 'react';
import { KeyOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Menu, Spin } from 'antd';
import { history, useModel } from 'umi';
import { outLogin } from '@/services/system/login';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import UserPhotoView from '@/pages/common/userPhoto/UserPhotoView';
import UpdatePwd from '@/components/RightContent/action/UpdatePwd';
import UpdateUserInfo from './action/UpdateUserInfo';

export interface GlobalHeaderRightProps {
  menu?: boolean;
}

/**
 * 退出登录，并且将当前的 url 保存
 */
const loginOut = async () => {
  await outLogin();
  const { query, pathname } = history.location;
  const { redirect } = query;
  // Note: There may be security issues, please note
  if (window.location.pathname !== '/user/login' && !redirect) {
    history.replace({
      pathname: '/user/login',
    });
  }
};

const AvatarDropdown: React.FC<GlobalHeaderRightProps> = ({ menu }) => {
  const { initialState, setInitialState } = useModel('@@initialState');

  const pwdRef = useRef();
  const userInfoRef = useRef();

  const onMenuClick = useCallback(
    (event: {
      key: React.Key;
      keyPath: React.Key[];
      item: React.ReactInstance;
      domEvent: React.MouseEvent<HTMLElement>;
    }) => {
      const { key } = event;
      if (key === 'logout' && initialState) {
        setInitialState({ ...initialState, currentUser: undefined });
        loginOut();
        return;
      } else if (key === 'pwd') {
        // 初始化
        pwdRef.current.onPreInit();
      } else if (key === 'center') {
        userInfoRef.current.onPreInit();
      }
      // history.push(`/account/${key}`);
    },
    [],
  );

  const loading = (
    <span className={`${styles.action} ${styles.account}`}>
      <Spin
        size="small"
        style={{
          marginLeft: 8,
          marginRight: 8,
        }}
      />
    </span>
  );

  if (!initialState) {
    return loading;
  }

  const { currentUser } = initialState;

  if (!currentUser || !currentUser.name) {
    return loading;
  }

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      <Menu.Item key="center">
        <UserOutlined />
        个人中心
      </Menu.Item>
      <Menu.Item key="pwd">
        <KeyOutlined />
        修改密码
      </Menu.Item>
      <Menu.Item key="logout">
        <LogoutOutlined />
        退出登录
      </Menu.Item>
    </Menu>
  );
  return (
    <>
      <HeaderDropdown overlay={menuHeaderDropdown}>
        <span className={`${styles.action} ${styles.account}`}>
          <UserPhotoView
            size={25}
            photoStr={currentUser.avatar}
            style={{ marginRight: 5, marginTop: -3 }}
          />
          <span className={`${styles.name} anticon`}>{currentUser.name}</span>
        </span>
      </HeaderDropdown>
      {/* 操作 */}
      <UpdateUserInfo ref={userInfoRef} />
      <UpdatePwd ref={pwdRef} />
    </>
  );
};

export default AvatarDropdown;
